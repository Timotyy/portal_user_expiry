Attribute VB_Name = "DADC_PurgingSystem_Purge_Clips"
Option Explicit
Dim g_strConnection As String
Dim g_strConnection2 As String

Const g_strSMTPServer = "jca-aspera.jcatv.co.uk"
Const g_strSMTPUserName = "" ' "aria@freenetname.co.uk"
Const g_strSMTPPassword = "" '"Chorale1"
Const g_strEmailFooter = "Media Window at RRsat Europe"
Const g_strFullUSerName = "RRSat Europe Operations"

Public g_strAdministratorEmailAddress As String
Public g_strAdministratorEmailName As String
Public g_strBookingsEmailAddress As String
Public g_strBookingsEmailName As String

Public cnn As ADODB.Connection, cnn2 As ADODB.Connection, rst As ADODB.Recordset
Public SQL As String, l_strCommandLine As String, l_strEmail As String, l_strEmailFooter As String, l_lngCompanyID As Long
Public l_datNow As Date, l_datExpiry As Date, l_lngExpiry As Long, l_strCetaClientCode As String, l_lngCount As Long
Public l_lngClipID As Long, l_strClipFilename As String, l_lngNewClipID As Long, l_strClipReference As String, FlagNoExpire As Boolean
Public FSO As Scripting.FileSystemObject, l_strFilePath As String, l_lngOldLibraryID As Long, l_lngStoreLibraryID As Long, l_strStoreBarcode As String, l_strStoreFilePath As String
Public l_strEmailTo As String, l_strEmailToName As String, l_strEmailCC As String, l_strEmailCCName As String
Public l_strEmailSubject As String, l_strEmailFileList As String
Public l_datExpiry1 As Date, l_datExpiry2 As Date

Sub Main()

Set FSO = New Scripting.FileSystemObject

Set cnn = New ADODB.Connection

l_strCommandLine = Command()
g_strConnection = "DRIVER={ODBC Driver 13 for SQL Server};SERVER=lon-bssql-01.office.refine-group.com;DATABASE=cetasoft;UID=Web_User;PWD=Web_User_Password1903"
cnn.Open g_strConnection

'Set up the default email varaibles for use with notification emails
SetDefaultEmailParameters

l_datNow = Format(Now, "dd/mmm/yy")

'Do DADC Manual Ingest Purging
Purge_DADC_Ingest

'Close up and finish
cnn.Close

Set cnn = Nothing
Beep

End Sub

Sub Purge_DADC_Ingest()

' For all billed tracker items that have not been purged
' 0) Check billed date
' 1) If greater than 30 days ago then
' 2) Delete the file
' 3) Mask the tracker item as purged

Dim l_rsPurgeQueue As ADODB.Recordset
Dim l_rsMasterfiles As ADODB.Recordset
Dim l_strSQL As String
Dim l_datRelevantDate As Date

Set l_rsPurgeQueue = New ADODB.Recordset
Set l_rsMasterfiles = New ADODB.Recordset

l_datRelevantDate = DateAdd("D", -30, Now)

l_strSQL = "SELECT tracker_dadc_itemID, itemreference, itemfilename, companyID FROM tracker_dadc_item WHERE billed <> 0 and file_purged = 0 and (stagefield15 < '" & Format(l_datRelevantDate, "YYYY-MM-DD HH:NN:SS") & _
"' OR stagefield13 < '" & Format(l_datRelevantDate, "YYYY-MM-DD HH:NN:SS") & "');"
Debug.Print l_strSQL
l_rsPurgeQueue.Open l_strSQL, cnn, 3, 3

If l_rsPurgeQueue.RecordCount > 0 Then
    l_rsPurgeQueue.MoveFirst
    Do While Not l_rsPurgeQueue.EOF
        l_strSQL = "SELECT eventID, libraryID, bigfilesize FROM events WHERE clipreference = '" & l_rsPurgeQueue("itemreference") & "' AND clipfilename = '" & l_rsPurgeQueue("itemfilename") & _
        "' AND companyID = " & l_rsPurgeQueue("companyID") & " AND altlocation <> 'COMPLETED JOBS\DADC_BBC\BBC_QUARANTINE_DO_NOT_DELETE' AND system_deleted = 0;"
        Debug.Print l_strSQL
        l_rsMasterfiles.Open l_strSQL, cnn, 3, 3
        If l_rsMasterfiles.RecordCount > 0 Then
            l_rsMasterfiles.MoveFirst
            Do While Not l_rsMasterfiles.EOF
                If GetData("library", "format", "LibraryID", l_rsMasterfiles("libraryID")) = "DISCSTORE" And Not IsNull(l_rsMasterfiles("bigfilesize")) And l_rsMasterfiles("bigfilesize") <> 0 Then
                    l_strSQL = "INSERT INTO event_file_request (eventID, event_file_request_typeID, newlibraryID, RequestName) VALUES ("
                    l_strSQL = l_strSQL & l_rsMasterfiles("eventID") & ", "
                    l_strSQL = l_strSQL & GetData("event_File_request_type", "event_file_request_typeID", "description", "Delete") & ", "
                    l_strSQL = l_strSQL & l_rsMasterfiles("libraryID") & ", "
                    l_strSQL = l_strSQL & "'DADC Purge');"
                    Debug.Print l_strSQL
                    cnn.Execute l_strSQL
                End If
                l_rsMasterfiles.MoveNext
            Loop
        End If
        SetData "tracker_dadc_item", "file_purged", "tracker_dadc_itemID", l_rsPurgeQueue("tracker_dadc_itemID"), 1
        l_rsMasterfiles.Close
        l_rsPurgeQueue.MoveNext
    Loop
End If
l_rsPurgeQueue.Close

Set l_rsMasterfiles = Nothing
Set l_rsPurgeQueue = Nothing

End Sub

Function GetData(lp_strTableName As String, lp_strFieldToReturn As String, lp_strFieldToSearch As String, lp_varCriterea As Variant) As Variant
        '<EhHeader>
        On Error GoTo GetData_Err
        '</EhHeader>


    Dim l_strSQL As String
    l_strSQL = "SELECT " & lp_strFieldToReturn & " FROM " & lp_strTableName & " WHERE " & lp_strFieldToSearch & " = '" & (lp_varCriterea) & "'"

    Dim l_rstGetData As New ADODB.Recordset
    Dim l_con As New ADODB.Connection

    l_con.Open g_strConnection
    l_rstGetData.Open l_strSQL, l_con, adOpenStatic, adLockReadOnly

    If Not l_rstGetData.EOF Then
    
        If Not IsNull(l_rstGetData(lp_strFieldToReturn)) Then
            GetData = l_rstGetData(lp_strFieldToReturn)
            GoTo Proc_CloseDB
        End If

    Else
        GoTo Proc_CloseDB

    End If

    Select Case l_rstGetData.Fields(lp_strFieldToReturn).Type
    Case adChar, adVarChar, adVarWChar, 201, 203
        GetData = ""
    Case adBigInt, adBinary, adBoolean, adCurrency, adDecimal, adDouble, adInteger
        GetData = 0
    Case adDate, adDBDate, adDBTime, adDBTimeStamp
        GetData = 0
    Case Else
        GetData = False
    End Select

Proc_CloseDB:

    On Error Resume Next

    l_rstGetData.Close
    Set l_rstGetData = Nothing

    l_con.Close
    Set l_con = Nothing


        '<EhFooter>
        Exit Function

GetData_Err:
        MsgBox Err.Description & vbCrLf & _
               "in prjJCAFileManager.modTest.GetData " & _
               "at line " & Erl, _
               vbExclamation + vbOKOnly, "Application Error"
        Resume Next
        '</EhFooter>
End Function

Function GetDataSQL(lp_strSQL As String) As Variant

On Error GoTo Proc_GetData_Error

Dim l_rstGetData As New ADODB.Recordset
Dim l_conGetData As New ADODB.Connection

l_conGetData.Open g_strConnection
l_rstGetData.Open lp_strSQL, l_conGetData, adOpenStatic, adLockReadOnly

If Not l_rstGetData.EOF Then
    
    If Not IsNull(l_rstGetData(0)) Then
        GetDataSQL = l_rstGetData(0)
    Else
        Select Case l_rstGetData.Fields(0).Type
        Case adChar, adVarChar, adVarWChar, 201
            GetDataSQL = ""
        Case adBigInt, adBinary, adBoolean, adCurrency, adDecimal, adDouble, adInteger
            GetDataSQL = 0
        Case adDate, adDBDate, adDBTime, adDBTimeStamp
            GetDataSQL = 0
        Case Else
            GetDataSQL = False
        End Select
    End If

End If

l_rstGetData.Close
Set l_rstGetData = Nothing

l_conGetData.Close
Set l_conGetData = Nothing

Exit Function

Proc_GetData_Error:

GetDataSQL = ""

End Function
Sub SendSMTPMail(ByVal lp_strEmailToAddress As String, ByVal lp_strEmailToName As String, ByVal lp_strSubject As String, ByVal lp_strBody As String, ByVal lp_strAttachment As String, _
ByVal lp_blnDontShowErrors As Boolean, ByVal lp_strCopyToEmailAddress As String, ByVal lp_strCopyToName As String, Optional ByVal lp_strBCCAddress As String, _
Optional lp_blnNoGlobalFooter As Boolean, Optional lp_strEmailFromAddress As String)

Dim SQL As String, cnn2 As ADODB.Connection

If lp_blnNoGlobalFooter = True Then
    'Nothing
Else
    lp_strBody = lp_strBody & vbCrLf & vbCrLf & g_strEmailFooter
End If

SQL = "INSERT INTO emailmessage (emailTo, emailCC, emailBCC, emailSubject, emailBodyPlain, emailAttachmentPath, emailFrom, cuser, muser, SendViaService) VALUES ("
SQL = SQL & "'" & QuoteSanitise(lp_strEmailToAddress) & "', "
SQL = SQL & IIf(lp_strCopyToEmailAddress <> "", "'" & QuoteSanitise(lp_strCopyToEmailAddress) & "', ", "Null, ")
SQL = SQL & IIf(lp_strBCCAddress <> "", "'" & QuoteSanitise(lp_strBCCAddress) & "', ", "Null, ")
SQL = SQL & "'" & QuoteSanitise(lp_strSubject) & "', "
SQL = SQL & "'" & QuoteSanitise(lp_strBody) & "', "
SQL = SQL & IIf(lp_strAttachment <> "", "'" & QuoteSanitise(lp_strAttachment) & "', ", "Null, ")
SQL = SQL & IIf(lp_strEmailFromAddress <> "", "'" & QuoteSanitise(lp_strEmailFromAddress) & "', ", "Null, ")
SQL = SQL & "'Aspera', "
SQL = SQL & "'Aspera', "
SQL = SQL & "1);"

'On Error Resume Next
Debug.Print SQL
Set cnn2 = New ADODB.Connection
cnn2.Open g_strConnection
cnn2.Execute SQL
cnn2.Close
Set cnn2 = Nothing
'On Error GoTo 0

End Sub

Function QuoteSanitise(lp_strText) As String
        '<EhHeader>
        On Error GoTo QuoteSanitise_Err
        '</EhHeader>


        Dim l_strNewText As String

        Dim l_strOldText As String

        Dim l_intLoop    As Integer

         l_strNewText = ""
    
         If IsNull(lp_strText) Then
            l_strOldText = ""
            l_strNewText = l_strOldText
            lp_strText = ""
        Else
            l_strOldText = lp_strText
        End If
    
        If InStr(lp_strText, "'") <> 0 Then
        
            For l_intLoop = 1 To Len(l_strOldText)
            
                If Mid(l_strOldText, l_intLoop, 1) = "'" Then
                    l_strNewText = l_strNewText & "''"
                Else
                    l_strNewText = l_strNewText & Mid(l_strOldText, l_intLoop, 1)
                End If
            
            Next

        Else
            l_strNewText = lp_strText
        End If
    
        If Right(l_strNewText, 1) = "/" Or Right(l_strNewText, 1) = "\" Then
            l_strNewText = Left(l_strNewText, Len(l_strNewText) - 1)
        End If
    
        QuoteSanitise = l_strNewText
    

        '<EhFooter>
        Exit Function

QuoteSanitise_Err:
        MsgBox Err.Description & vbCrLf & _
               "in prjJCAFileManager.modTest.QuoteSanitise " & _
               "at line " & Erl, _
               vbExclamation + vbOKOnly, "Application Error"
        Resume Next
        '</EhFooter>
End Function

Public Function FormatSQLDate(lDate As Variant)
    
    'TVCodeTools ErrorEnablerStart
    On Error GoTo PROC_ERR
    'TVCodeTools ErrorEnablerEnd
    
    If IsNull(lDate) Then
        FormatSQLDate = Null
        Exit Function
    End If
    If lDate = "" Then
        FormatSQLDate = Null
        Exit Function
    End If
    If Not IsDate(lDate) Then
        FormatSQLDate = Null
        Exit Function
    
    End If
    
    FormatSQLDate = Year(lDate) & "/" & Month(lDate) & "/" & Day(lDate) & " " & Hour(lDate) & ":" & Minute(lDate) & ":" & Second(lDate)
    
    'TVCodeTools ErrorHandlerStart
PROC_EXIT:
    Exit Function
    
PROC_ERR:
    MsgBox Err.Description
    Resume PROC_EXIT
    'TVCodeTools ErrorHandlerEnd
    
End Function

Public Sub SetDefaultEmailParameters()

Dim rst As ADODB.Recordset

Set rst = New ADODB.Recordset

SQL = "SELECT value FROM setting WHERE name = 'AdministratorEmailAddress';"
rst.Open SQL, cnn, 3, 3
If rst.RecordCount > 0 Then
    g_strAdministratorEmailAddress = rst("value")
End If
rst.Close
SQL = "SELECT value FROM setting WHERE name = 'AdministratorEmailName';"
rst.Open SQL, cnn, 3, 3
If rst.RecordCount > 0 Then
    g_strAdministratorEmailName = rst("value")
End If
rst.Close
SQL = "SELECT value FROM setting WHERE name = 'BookingsEmailAddress';"
rst.Open SQL, cnn, 3, 3
If rst.RecordCount > 0 Then
    g_strBookingsEmailAddress = rst("value")
End If
rst.Close
SQL = "SELECT value FROM setting WHERE name = 'BookingsEmailName';"
rst.Open SQL, cnn, 3, 3
If rst.RecordCount > 0 Then
    g_strBookingsEmailName = rst("value")
End If
rst.Close

End Sub

Function SetData(lp_strTableName As String, lp_strFieldToEdit As String, lp_strFieldToSearch As String, lp_varCriterea As Variant, lp_varValue As Variant) As Variant

Dim l_strSQL As String, l_UpdateValue As String, c As ADODB.Connection

If UCase(lp_varValue) = "NULL" Or lp_varValue = "" Then
    l_UpdateValue = "NULL"
Else
    l_UpdateValue = "'" & QuoteSanitise(lp_varValue) & "'"
End If

l_strSQL = "UPDATE " & lp_strTableName & " SET " & lp_strFieldToEdit & " = " & l_UpdateValue & " WHERE " & lp_strFieldToSearch & " = '" & QuoteSanitise(lp_varCriterea) & "'"

Set c = New ADODB.Connection
c.Open g_strConnection

c.Execute l_strSQL
c.Close
Set c = Nothing

End Function

Public Function CopyEventToLibraryID(lp_lngEventID As Long, lp_lngLibraryID As Long) As Long

'copy the info from one job to another
Dim l_rstOriginalEvent As ADODB.Recordset
Dim l_rstNewEvent As ADODB.Recordset, l_lngNewEventID As Long
Dim c As ADODB.Connection
Dim l_rstTemp As ADODB.Recordset, l_lngTechrevID As Long

'get the original job's details
Dim l_strSQL As String

Set c = New ADODB.Connection
c.Open g_strConnection

l_strSQL = "INSERT INTO events ("
l_strSQL = l_strSQL & "eventmediatype, libraryID, companyID) VALUES ("
l_strSQL = l_strSQL & "'FILE', " & lp_lngLibraryID & ", 1);SELECT SCOPE_IDENTITY();"

Debug.Print l_strSQL
Set l_rstOriginalEvent = New ADODB.Recordset
Set l_rstOriginalEvent = c.Execute(l_strSQL)

l_lngNewEventID = l_rstOriginalEvent.NextRecordset().Fields(0).Value

l_strSQL = "SELECT * FROM events WHERE eventID = '" & lp_lngEventID & "';"
l_rstOriginalEvent.Open l_strSQL, c, adOpenDynamic, adLockBatchOptimistic

'allow adding of a new job
l_strSQL = "SELECT * FROM events WHERE eventID = '" & l_lngNewEventID & "';"
Set l_rstNewEvent = New ADODB.Recordset
l_rstNewEvent.Open l_strSQL, c, adOpenDynamic, adLockOptimistic

Dim l_intLoop As Integer
Dim l_lngConflict As Long
    
If l_rstOriginalEvent.EOF Then Exit Function

l_rstNewEvent("libraryID") = lp_lngLibraryID

For l_intLoop = 0 To l_rstOriginalEvent.Fields.Count - 1
    If l_rstOriginalEvent.Fields(l_intLoop).Name <> "libraryID" And l_rstOriginalEvent.Fields(l_intLoop).Name <> "eventID" Then
    
        If Not IsNull(l_rstOriginalEvent(l_intLoop)) Then
            l_rstNewEvent(l_intLoop) = l_rstOriginalEvent(l_intLoop)
        End If
    
    End If
Next

l_rstNewEvent("mdate") = Null
l_rstNewEvent("muser") = Null

l_rstNewEvent.Update

'MsgBox g_lngLastID, vbExclamation

'close the original record
l_rstOriginalEvent.Close

'close the new record
l_rstNewEvent.Close

Set l_rstOriginalEvent = Nothing
Set l_rstNewEvent = Nothing

CopyEventToLibraryID = l_lngNewEventID

End Function

