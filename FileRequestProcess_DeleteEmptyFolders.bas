Attribute VB_Name = "FileRequestProcess_DeleteEmptyFolders"
Option Explicit
Public g_strCetaConnection As String
Public cnn As ADODB.Connection
Public g_strUserEmailAddress As String
Public g_strUserEmailName As String
Public g_strAdministratorEmailAddress As String

Const g_strSMTPServer = "jca-eset.jcatv.co.uk"
Const g_strSMTPUserName = "" ' "aria@freenetname.co.uk"
Const g_strSMTPPassword = "" '"Chorale1"
Const g_strEmailFooter = ""
Const g_strFullUserName = "MX1 London Operations"

Private Declare Function GetComputerName Lib "kernel32" _
   Alias "GetComputerNameA" (ByVal lpBuffer As String, _
   nSize As Long) As Long
   
Public Const MAX_COMPUTERNAME_LENGTH As Long = 15&
Private Declare Sub Sleep Lib "kernel32" (ByVal dwMilliseconds As Long)

Public Declare Function GetDiskFreeSpaceEx Lib "kernel32.dll" Alias "GetDiskFreeSpaceExA" (ByVal lpDirectoryName As String, lpFreeBytesAvailableToCaller As Currency, lpTotalNumberOfBytes As Currency, lpTotalNumberofFreeBytes As Currency) As Long

Public Sub RunFileRequestProcess()

Dim FreeBytesAvailableToCaller As Currency, TotalNumberOfBytes As Currency, TotalNumberofFreeBytes As Currency, l_curSuccess As Currency

Dim l_rstRequests As ADODB.Recordset, l_lngClipID As Long, l_lngNewClipID As Long, l_lngLibraryID As Long, l_lngNewLibraryID As Long, l_strPathToFile As String, l_strNewPathToFile As String
Dim SQL As String, l_strCommandLine As String, l_strResult As String, l_lngFileHandle As Long, l_curFileSize As Currency
Dim l_datNow As Date, l_strCetaClientCode As String, l_rstExtras As ADODB.Recordset, l_strClipFormat As String
Dim FSO As Scripting.FileSystemObject, l_strFilename As String, l_strReference As String, l_strNewPath As String, l_strNewFileName As String, l_lngWriteCount As Long, l_lngRepeatcount As Long, l_lngCount As Long
Dim l_strComputerName As String, Sub_Path As String, Rebuilt_path As String, Email_Message As String, l_rstEmail As ADODB.Recordset
Dim myObject As Ceta_Checksum_Library.ChecksumClass
Dim Success As Boolean
Dim l_strNewFolder As String, l_strNewReference As String, l_strFileList As String
Dim l_lngJobID As Long, l_rstContacts As ADODB.Recordset, l_rstCCUsers As ADODB.Recordset, l_lngPortalUserID As Long, l_lngAsperaLibraryID As Long, l_strAsperaFolder As String
Dim l_strCCEmailList As String, l_strEmailBody As String
Dim l_rstClips As ADODB.Recordset, l_lngVerifyGoodFiles As Long, l_lngVerifyFailedFiles As Long, l_blnNotMovedFlag As Boolean, Message As String
Dim l_intCount As Long, l_intHours As Long
Dim l_strErrorDescription As String, RequestNumber As Long
Dim l_strChecksumType As String
Dim l_intUrgent As Long
Dim l_lngRelevantJobs As Long, l_lngChecksumRequestID As Long, rsIdentity As ADODB.Recordset
Dim LockFlag As Long

Set FSO = New Scripting.FileSystemObject

cnn.Open g_strCetaConnection

LockFlag = GetData("setting", "value", "name", "LockSystem")

If (LockFlag And 16) = 16 Then
    cnn.Close
    Exit Sub
End If

Set l_rstRequests = New ADODB.Recordset

SQL = "SELECT value FROM setting WHERE name = 'OperationsEmailAddress';"
l_rstRequests.Open SQL, cnn, 3, 3
If l_rstRequests.RecordCount > 0 Then
    g_strUserEmailAddress = l_rstRequests("value")
End If
l_rstRequests.Close
SQL = "SELECT value FROM setting WHERE name = 'OperationsEmailName';"
l_rstRequests.Open SQL, cnn, 3, 3
If l_rstRequests.RecordCount > 0 Then
    g_strUserEmailName = l_rstRequests("value")
End If
l_rstRequests.Close
SQL = "SELECT value FROM setting WHERE name = 'AdministratorEmailAddress';"
l_rstRequests.Open SQL, cnn, 3, 3
If l_rstRequests.RecordCount > 0 Then
    g_strAdministratorEmailAddress = l_rstRequests("value")
End If
l_rstRequests.Close

l_strComputerName = CurrentMachineName()
l_lngWriteCount = 0

Do

    On Error GoTo UNEXPECTED_ERROR
    DoEvents
    SQL = "SELECT TOP 1 * FROM Event_File_Request " & _
    "WHERE RequestComplete IS NULL AND RequestFailed IS NULL AND RequestStarted IS NULL " & _
    "AND (Event_File_Request_TypeID IN (SELECT event_file_request_TypeID FROM event_file_request_type WHERE description = 'Clear Empty Folders')) " & _
    "ORDER BY CASE WHEN Urgent <> 0 THEN 0 ELSE 1 END, RequestDate;"
                
    Debug.Print SQL
    
    l_rstRequests.Open SQL, cnn, 3, 3
    If l_rstRequests.RecordCount = 1 Then
    
        'The Payload is in here
        RequestNumber = l_rstRequests("Event_file_requestID")
        l_rstRequests.Close
        l_lngLibraryID = GetData("event_file_request", "NewLibraryID", "event_file_requestID", RequestNumber)
        
        If l_lngLibraryID <> 0 And GetData("event_file_request", "NewAltLocation", "event_file_requestID", RequestNumber) <> "" Then
            If GetDataSQL("SELECT subtitle from LIBRARY WHERE libraryID = " & l_lngLibraryID & " AND format = 'DISCSTORE';") <> "" Then
                
                l_strNewPath = GetDataSQL("SELECT subtitle from LIBRARY WHERE libraryID = " & l_lngLibraryID & " AND format = 'DISCSTORE';")
                l_strNewPath = l_strNewPath & "\" & GetData("event_file_request", "NewAltLocation", "event_file_requestID", RequestNumber)
                l_strCommandLine = "Delete_Empty_Dirs.exe """ & l_strNewPath & """"
                Debug.Print l_strCommandLine
                Shell l_strCommandLine
            End If
            
            SetData "event_file_request", "RequestStarted", "event_file_requestID", RequestNumber, Format(Now, "YYYY-MM-DD HH:NN:SS")
            SetData "event_file_request", "Handler", "event_file_requestID", RequestNumber, l_strComputerName
            SetData "event_file_request", "RequestComplete", "event_file_requestID", RequestNumber, Format(Now, "YYYY-MM-DD HH:NN:SS")
            
        Else
            App.LogEvent "Error: Malformed Request." & vbCrLf
            MsgBox "Error: Malformed Request"
            SetData "event_file_request", "RequestFailed", "event_file_requestID", RequestNumber, Format(Now, "YYYY-MM-DD HH:NN:SS")
            SetData "event_file_request", "Comment", "event_file_requestID", RequestNumber, "Disk Full"
            If Trim(" " & GetData("event_file_request", "RequesterEmail", "event_file_requestID", RequestNumber)) <> "" Then
                SendSMTPMail GetData("event_file_request", "RequesterEmail", "event_file_requestID", RequestNumber), "", "CETA File Request Failed", "Request: " & RequestNumber & vbCrLf & "Disk Full" & vbCrLf, "", True, "", "", g_strAdministratorEmailAddress
            End If
            Set l_rstRequests = Nothing
            cnn.Close
            Exit Sub
        
        End If
        
    End If
    
    SQL = "SELECT Count(Event_File_RequestID) FROM Event_File_Request " & _
    "WHERE RequestComplete IS NULL AND RequestFailed IS NULL AND RequestStarted IS NULL " & _
    "AND (Event_File_Request_TypeID IN (SELECT event_file_request_TypeID FROM event_file_request_type WHERE description = 'Clear Empty Folders'));"
        
    Debug.Print SQL
    
    l_rstRequests.Open SQL, cnn, 3, 3
    If l_rstRequests.RecordCount > 0 Then
        l_lngRelevantJobs = l_rstRequests(0)
        l_rstRequests.Close
    Else
        l_lngRelevantJobs = 0
        l_rstRequests.Close
    End If

    On Error GoTo 0
    
Loop While l_lngRelevantJobs > 0

Set l_rstRequests = Nothing

cnn.Close
Exit Sub

UNEXPECTED_ERROR:
App.LogEvent "Error: " & Err.Description & vbCrLf & "Write Count: " & l_lngWriteCount
MsgBox "Error: " & Err.Description
l_strErrorDescription = Err.Description
Resume Next

End Sub

Function QuoteSanitise(lp_strText) As String
        '<EhHeader>
        On Error GoTo QuoteSanitise_Err
        '</EhHeader>


        Dim l_strNewText As String

        Dim l_strOldText As String

        Dim l_intLoop    As Integer

         l_strNewText = ""
    
         If IsNull(lp_strText) Then
            l_strOldText = ""
            l_strNewText = l_strOldText
            lp_strText = ""
        Else
            l_strOldText = lp_strText
        End If
    
        If InStr(lp_strText, "'") <> 0 Then
        
            For l_intLoop = 1 To Len(l_strOldText)
            
                If Mid(l_strOldText, l_intLoop, 1) = "'" Then
                    l_strNewText = l_strNewText & "''"
                Else
                    l_strNewText = l_strNewText & Mid(l_strOldText, l_intLoop, 1)
                End If
            
            Next

        Else
            l_strNewText = lp_strText
        End If
    
        If Right(l_strNewText, 1) = "/" Or Right(l_strNewText, 1) = "\" Then
            l_strNewText = Left(l_strNewText, Len(l_strNewText) - 1)
        End If
    
        QuoteSanitise = l_strNewText
    

        '<EhFooter>
        Exit Function

QuoteSanitise_Err:
        MsgBox Err.Description & vbCrLf & _
               "in prjJCAFileManager.modTest.QuoteSanitise " & _
               "at line " & Erl, _
               vbExclamation + vbOKOnly, "Application Error"
        Resume Next
        '</EhFooter>
End Function

Function GetData(lp_strTableName As String, lp_strFieldToReturn As String, lp_strFieldToSearch As String, lp_varCriterea As Variant, Optional lp_ReturnTrueNulls As Boolean) As Variant

Dim l_strSQL As String

l_strSQL = "SELECT " & lp_strFieldToReturn & " FROM " & lp_strTableName & " WHERE " & lp_strFieldToSearch & " = '" & (lp_varCriterea) & "'"

Dim l_rstGetData As New ADODB.Recordset
Dim l_con As New ADODB.Connection

l_con.Open g_strCetaConnection
l_rstGetData.Open l_strSQL, l_con, adOpenStatic, adLockReadOnly

If Not l_rstGetData.EOF Then

    If Not IsNull(l_rstGetData(lp_strFieldToReturn)) Then
        GetData = l_rstGetData(lp_strFieldToReturn)
        GoTo Proc_CloseDB
    End If

Else
    GoTo Proc_CloseDB

End If

If lp_ReturnTrueNulls = True Then
    GetData = Null
Else
    Select Case l_rstGetData.Fields(lp_strFieldToReturn).Type
    Case adChar, adVarChar, adVarWChar, 201, 203
        GetData = ""
    Case adBigInt, adBinary, adBoolean, adCurrency, adDecimal, adDouble, adInteger
        GetData = 0
    Case adDate, adDBDate, adDBTime, adDBTimeStamp
        GetData = 0
    Case Else
        GetData = False
    End Select
End If

Proc_CloseDB:

On Error Resume Next

l_rstGetData.Close
Set l_rstGetData = Nothing

l_con.Close
Set l_con = Nothing

Exit Function

End Function

Function GetDataSQL(lp_strSQL As String)

On Error GoTo Proc_GetData_Error

Dim l_rstGetData As New ADODB.Recordset
Dim l_conGetData As New ADODB.Connection

l_conGetData.Open g_strCetaConnection
l_rstGetData.Open lp_strSQL, l_conGetData, 3, 3

If Not l_rstGetData.EOF Then
    
    If Not IsNull(l_rstGetData(0)) Then
        GetDataSQL = l_rstGetData(0)
    Else
        Select Case l_rstGetData.Fields(0).Type
        Case adChar, adVarChar, adVarWChar, 201
            GetDataSQL = ""
        Case adBigInt, adBinary, adBoolean, adCurrency, adDecimal, adDouble, adInteger
            GetDataSQL = 0
        Case adDate, adDBDate, adDBTime, adDBTimeStamp
            GetDataSQL = 0
        Case Else
            GetDataSQL = False
        End Select
    End If

End If

l_rstGetData.Close
Set l_rstGetData = Nothing

l_conGetData.Close
Set l_conGetData = Nothing

Exit Function

Proc_GetData_Error:

GetDataSQL = ""

End Function

Function SetData(lp_strTableName As String, lp_strFieldToEdit As String, lp_strFieldToSearch As String, lp_varCriterea As Variant, lp_varValue As Variant) As Variant

Dim l_strSQL As String, l_UpdateValue As String, c As ADODB.Connection

If UCase(lp_varValue) = "NULL" Or lp_varValue = "" Then
    l_UpdateValue = "NULL"
Else
    l_UpdateValue = "'" & QuoteSanitise(lp_varValue) & "'"
End If

l_strSQL = "UPDATE " & lp_strTableName & " SET " & lp_strFieldToEdit & " = " & l_UpdateValue & " WHERE " & lp_strFieldToSearch & " = '" & QuoteSanitise(lp_varCriterea) & "'"

Set c = New ADODB.Connection
c.Open g_strCetaConnection

c.Execute l_strSQL
c.Close
Set c = Nothing

End Function

Public Function FormatSQLDate(lDate As Variant, Optional lp_blnMySQL As Boolean)
    
    'TVCodeTools ErrorEnablerStart
    On Error GoTo PROC_ERR
    'TVCodeTools ErrorEnablerEnd
    
    If IsNull(lDate) Then
        FormatSQLDate = Null
        Exit Function
    End If
    If lDate = "" Then
        FormatSQLDate = Null
        Exit Function
    End If
    If Not IsDate(lDate) Then
        FormatSQLDate = Null
        Exit Function
    
    End If
    
    If lp_blnMySQL = True Then
        FormatSQLDate = Year(lDate) & "/" & Month(lDate) & "/" & Day(lDate) & " " & Format(Hour(lDate), "00") & ":" & Format(Minute(lDate), "00") & ":" & Format(Second(lDate), "00")
    Else
        FormatSQLDate = Month(lDate) & "/" & Day(lDate) & "/" & Year(lDate) & " " & Hour(lDate) & ":" & Minute(lDate) & ":" & Second(lDate)
    End If
    
    'TVCodeTools ErrorHandlerStart
PROC_EXIT:
    Exit Function
    
PROC_ERR:
    MsgBox Err.Description
    Resume PROC_EXIT
    'TVCodeTools ErrorHandlerEnd
    
End Function

Function GetNextSequence(lp_strSequenceName As String) As Long
    
    'TVCodeTools ErrorEnablerStart
    On Error GoTo PROC_ERR
    'TVCodeTools ErrorEnablerEnd
    
    Dim l_strSQL As String, c As ADODB.Connection
    Dim l_lngGetNextSequence  As Long
    l_strSQL = "SELECT * FROM sequence WHERE sequencename = '" & lp_strSequenceName & "';"
    
    Set c = New ADODB.Connection
    c.Open g_strCetaConnection
    Dim l_rsSequence As ADODB.Recordset
    Set l_rsSequence = New ADODB.Recordset
    l_rsSequence.Open l_strSQL, c, 3, 3
    
    If Not l_rsSequence.EOF Then
        l_rsSequence.MoveFirst
        l_lngGetNextSequence = l_rsSequence("sequencevalue")
                
        l_strSQL = "UPDATE sequence SET sequencevalue = '" & l_lngGetNextSequence + 1 & "', muser = 'ASPERA', mdate = '" & FormatSQLDate(Now()) & "' WHERE sequencename = '" & lp_strSequenceName & "';"
        
        c.Execute l_strSQL
        
    Else
        
        Dim l_strSequenceNumber As String
PROC_Input_Number:
        l_strSequenceNumber = InputBox("There is currently no default value for a sequence of " & lp_strSequenceName & ". Please specify the initial value.", "No default value")
        If Not IsNumeric(l_strSequenceNumber) Then
            MsgBox "Please enter a valid numeric, positive integer.", vbExclamation
            GoTo PROC_Input_Number
        Else
            l_rsSequence.AddNew
            l_rsSequence("sequencename") = lp_strSequenceName
            l_rsSequence("sequencevalue") = Val(l_strSequenceNumber) + 1
            l_rsSequence("muser") = "ASPERA"
            l_rsSequence("mdate") = Now()
            l_rsSequence.Update
            l_lngGetNextSequence = Val(l_strSequenceNumber)
        End If
    End If
    
    l_rsSequence.Close
    Set l_rsSequence = Nothing
    
    c.Close
    Set c = Nothing
    GetNextSequence = l_lngGetNextSequence
    
    'TVCodeTools ErrorHandlerStart
PROC_EXIT:
    Exit Function
    
PROC_ERR:
    MsgBox Err.Description
    Resume PROC_EXIT
    'TVCodeTools ErrorHandlerEnd
    
End Function

Public Function CopyEventToLibraryID(lp_lngEventID As Long, lp_lngLibraryID As Long) As Long

'copy the info from one job to another
Dim l_rstOriginalEvent As ADODB.Recordset
Dim l_rstNewEvent As ADODB.Recordset, l_lngNewEventID As Long
Dim c As ADODB.Connection
Dim l_rstTemp As ADODB.Recordset, l_lngTechrevID As Long

'get the original job's details
Dim l_strSQL As String

Set c = New ADODB.Connection
c.Open g_strCetaConnection

l_strSQL = "INSERT INTO events ("
l_strSQL = l_strSQL & "eventmediatype, libraryID, companyID) VALUES ("
l_strSQL = l_strSQL & "'FILE', " & lp_lngLibraryID & ", 1);SELECT SCOPE_IDENTITY();"

Debug.Print l_strSQL
Set l_rstOriginalEvent = New ADODB.Recordset
Set l_rstOriginalEvent = c.Execute(l_strSQL)

l_lngNewEventID = l_rstOriginalEvent.NextRecordset().Fields(0).Value

l_strSQL = "SELECT * FROM events WHERE eventID = '" & lp_lngEventID & "';"
l_rstOriginalEvent.Open l_strSQL, c, adOpenDynamic, adLockBatchOptimistic

'allow adding of a new job
l_strSQL = "SELECT * FROM events WHERE eventID = '" & l_lngNewEventID & "';"
Set l_rstNewEvent = New ADODB.Recordset
l_rstNewEvent.Open l_strSQL, c, adOpenDynamic, adLockOptimistic

Dim l_intLoop As Integer
Dim l_lngConflict As Long
    
If l_rstOriginalEvent.EOF Then Exit Function

l_rstNewEvent("libraryID") = lp_lngLibraryID

For l_intLoop = 0 To l_rstOriginalEvent.Fields.Count - 1
    If l_rstOriginalEvent.Fields(l_intLoop).Name <> "libraryID" And l_rstOriginalEvent.Fields(l_intLoop).Name <> "eventID" And l_rstOriginalEvent.Fields(l_intLoop).Name <> "bigfilesize" And l_rstOriginalEvent.Fields(l_intLoop).Name <> "MediaWindowDeleteDate" Then
    
        If Not IsNull(l_rstOriginalEvent(l_intLoop)) Then
            l_rstNewEvent(l_intLoop) = l_rstOriginalEvent(l_intLoop)
        End If
    
    End If
Next

l_rstNewEvent("mdate") = Null
l_rstNewEvent("muser") = Null

l_rstNewEvent.Update

'MsgBox g_lngLastID, vbExclamation

'close the original record
l_rstOriginalEvent.Close

'close the new record
l_rstNewEvent.Close

'Copy the Segment Information re-using the l_rstOriginalEvent to read the records
l_rstOriginalEvent.Open "SELECT * FROM eventsegment WHERE eventID = " & lp_lngEventID, c, 3, 3
If l_rstOriginalEvent.RecordCount > 0 Then
    l_rstOriginalEvent.MoveFirst
    Do While Not l_rstOriginalEvent.EOF
        l_strSQL = "INSERT INTO eventsegment (eventID, segmentreference, timecodestart, timecodestop, note) VALUES ("
        l_strSQL = l_strSQL & l_lngNewEventID & ", "
        l_strSQL = l_strSQL & "'" & QuoteSanitise(l_rstOriginalEvent("segmentreference")) & "', "
        l_strSQL = l_strSQL & "'" & l_rstOriginalEvent("timecodestart") & "', "
        l_strSQL = l_strSQL & "'" & l_rstOriginalEvent("timecodestop") & "', "
        l_strSQL = l_strSQL & "'" & QuoteSanitise(l_rstOriginalEvent("note")) & "');"
        c.Execute l_strSQL
        l_rstOriginalEvent.MoveNext
    Loop
End If
l_rstOriginalEvent.Close

'Copy the Logging Information re-using the l_rstOriginalEvent to read the records
l_rstOriginalEvent.Open "SELECT * FROM eventlogging WHERE eventID = " & lp_lngEventID, c, 3, 3
If l_rstOriginalEvent.RecordCount > 0 Then
    l_rstOriginalEvent.MoveFirst
    Do While Not l_rstOriginalEvent.EOF
        l_strSQL = "INSERT INTO eventlogging (eventID, segmentreference, timecodestart, timecodestop, note) VALUES ("
        l_strSQL = l_strSQL & l_lngNewEventID & ", "
        l_strSQL = l_strSQL & "'" & QuoteSanitise(l_rstOriginalEvent("segmentreference")) & "', "
        l_strSQL = l_strSQL & "'" & l_rstOriginalEvent("timecodestart") & "', "
        l_strSQL = l_strSQL & "'" & l_rstOriginalEvent("timecodestop") & "', "
        l_strSQL = l_strSQL & "'" & QuoteSanitise(l_rstOriginalEvent("note")) & "');"
        c.Execute l_strSQL
        l_rstOriginalEvent.MoveNext
    Loop
End If
l_rstOriginalEvent.Close

'Copy the iTunes Advisory Information
l_strSQL = "SELECT * FROM iTunes_Clip_Advisory WHERE eventID = " & lp_lngEventID & ";"
l_rstOriginalEvent.Open l_strSQL, c, 3, 3
If l_rstOriginalEvent.RecordCount > 0 Then
    l_rstOriginalEvent.MoveFirst
    Do While Not l_rstOriginalEvent.EOF
        l_strSQL = "INSERT INTO iTunes_Clip_Advisory (eventID, advisorysystem, advisory) VALUES ("
        l_strSQL = l_strSQL & l_lngNewEventID & ", "
        l_strSQL = l_strSQL & "'" & QuoteSanitise(l_rstOriginalEvent("advisorysystem")) & "', "
        l_strSQL = l_strSQL & "'" & QuoteSanitise(l_rstOriginalEvent("advisory")) & "');"
        c.Execute l_strSQL
        l_rstOriginalEvent.MoveNext
    Loop
End If
l_rstOriginalEvent.Close

'Copy the iTunes Product Details
l_strSQL = "SELECT * FROM iTunes_product WHERE eventID = " & lp_lngEventID & ";"
l_rstOriginalEvent.Open l_strSQL, c, 3, 3
If l_rstOriginalEvent.RecordCount > 0 Then
    l_rstOriginalEvent.MoveFirst
    Do While Not l_rstOriginalEvent.EOF
        l_strSQL = "INSERT INTO iTunes_product (eventID, territory, salesstartdate, clearedforsale) VALUES ("
        l_strSQL = l_strSQL & l_lngNewEventID & ", "
        l_strSQL = l_strSQL & "'" & QuoteSanitise(l_rstOriginalEvent("territory")) & "', "
        l_strSQL = l_strSQL & "'" & FormatSQLDate(l_rstOriginalEvent("salesstartdate")) & "', "
        l_strSQL = l_strSQL & "'" & QuoteSanitise(l_rstOriginalEvent("clearedforsale")) & "');"
        Debug.Print l_strSQL
        c.Execute l_strSQL
        l_rstOriginalEvent.MoveNext
    Loop
End If
l_rstOriginalEvent.Close

Set l_rstOriginalEvent = Nothing
Set l_rstNewEvent = Nothing

CopyEventToLibraryID = l_lngNewEventID

End Function

Public Function CurrentMachineName() As String
            
    'TVCodeTools ErrorEnablerStart
    On Error GoTo PROC_ERR
    'TVCodeTools ErrorEnablerEnd
    
    Dim lSize As Long
    Dim sBuffer As String
    sBuffer = Space$(MAX_COMPUTERNAME_LENGTH + 1)
    lSize = Len(sBuffer)
    
    If GetComputerName(sBuffer, lSize) Then
        CurrentMachineName = Left$(sBuffer, lSize)
    End If
    
    'TVCodeTools ErrorHandlerStart
PROC_EXIT:
    Exit Function
    
PROC_ERR:
    MsgBox Err.Description
    Resume PROC_EXIT
    'TVCodeTools ErrorHandlerEnd
    
End Function

Public Function CheckLocal10Gig() As Boolean

Dim IPConfig As Variant
Dim IPConfigSet As Object

Set IPConfigSet = GetObject("winmgmts:{impersonationLevel=impersonate}").ExecQuery("SELECT IPAddress FROM Win32_NetworkAdapterConfiguration WHERE IPEnabled = TRUE")

CheckLocal10Gig = False

For Each IPConfig In IPConfigSet
    If Not IsNull(IPConfig.ipaddress) Then
        If InStr(Trim(" " & IPConfig.ipaddress(0)), "10.9.7.") > 0 Or InStr(Trim(" " & IPConfig.ipaddress(0)), "10.9.77.") > 0 Then
            CheckLocal10Gig = True
        End If
    End If
Next IPConfig

End Function

Sub SendSMTPMail(ByVal lp_strEmailToAddress As String, ByVal lp_strEmailToName As String, ByVal lp_strSubject As String, ByVal lp_strBody As String, ByVal lp_strAttachment As String, _
ByVal lp_blnDontShowErrors As Boolean, ByVal lp_strCopyToEmailAddress As String, ByVal lp_strCopyToName As String, Optional ByVal lp_strBCCAddress As String, _
Optional lp_blnNoGlobalFooter As Boolean, Optional lp_strEmailFromAddress As String)

Dim SQL As String, cnn2 As ADODB.Connection

If lp_blnNoGlobalFooter = True Then
    'Nothing
Else
    lp_strBody = lp_strBody & vbCrLf & vbCrLf & g_strEmailFooter
End If

SQL = "INSERT INTO emailmessage (emailTo, emailCC, emailBCC, emailSubject, emailBodyPlain, emailAttachmentPath, emailFrom, cuser, muser, SendViaService) VALUES ("
SQL = SQL & "'" & QuoteSanitise(lp_strEmailToAddress) & "', "
SQL = SQL & IIf(lp_strCopyToEmailAddress <> "", "'" & QuoteSanitise(lp_strCopyToEmailAddress) & "', ", "Null, ")
SQL = SQL & IIf(lp_strBCCAddress <> "", "'" & QuoteSanitise(lp_strBCCAddress) & "', ", "Null, ")
SQL = SQL & "'" & QuoteSanitise(lp_strSubject) & "', "
SQL = SQL & "'" & QuoteSanitise(lp_strBody) & "', "
SQL = SQL & IIf(lp_strAttachment <> "", "'" & QuoteSanitise(lp_strAttachment) & "', ", "Null, ")
SQL = SQL & IIf(lp_strEmailFromAddress <> "", "'" & QuoteSanitise(lp_strEmailFromAddress) & "', ", "Null, ")
SQL = SQL & "'Aspera', "
SQL = SQL & "'Aspera', "
SQL = SQL & "1);"

'On Error Resume Next
Debug.Print SQL
Set cnn2 = New ADODB.Connection
cnn2.Open g_strCetaConnection
cnn2.Execute SQL
cnn2.Close
Set cnn2 = Nothing
'On Error GoTo 0

End Sub

Function FTPUpload(lp_strRemoteFilename As String, lp_strLocalFile As String, Optional lp_strRemoteDirectory As String) As Boolean

Dim Success As Integer, Count As Integer
Dim ftp As New ChilkatFtp2

' Any string unlocks the component for the 1st 30-days.
Success = ftp.UnlockComponent("JCATVrFTP_Wbz2YPz5lzup")
If (Success <> 1) Then
    FTPUpload = False
    Exit Function
End If

ftp.HostName = GetData("setting", "value", "name", "MX1SmartjogFTPhostname")
ftp.Username = GetData("setting", "value", "name", "MX1SmartjogFTPusername")
ftp.Password = GetData("setting", "value", "name", "MX1SmartjogFTPpassword")

ftp.Passive = 1

' The default data transfer mode is "Active" as opposed to "Passive".

' Connect and login to the FTP server.
Success = 0: Count = 0
While Success <> 1 And Count < 10
    Success = ftp.Connect()
    If (Success <> 1) Then
        FTPUpload = False
        Count = Count + 1
    End If
Wend
If Success <> 1 Then Exit Function

' Change to the remote directory where the file is located.
' This step is only necessary if the file is not in the root directory
' for the FTP account.
If Not IsEmpty(lp_strRemoteDirectory) And lp_strRemoteDirectory <> "/" Then
    Success = ftp.ChangeRemoteDir(lp_strRemoteDirectory)
    If (Success <> 1) Then
        FTPUpload = False
        Exit Function
    End If
End If

' Upload a file.
Success = ftp.PutFile(lp_strLocalFile, lp_strRemoteFilename)
If (Success <> 1) Then
    FTPUpload = False
    Exit Function
End If

ftp.Disconnect
FTPUpload = True

End Function

Function VerifyFile(lp_lngEventID As Long, lp_lngLibraryID As Long) As Boolean

Dim l_strPathToFile As String, l_strFilename As String, l_curFileSize As Currency, l_lngFileHandle As Long, FSO As Scripting.FileSystemObject

If CheckLocal10Gig = True Then
    l_strPathToFile = GetData("library", "foreignref", "libraryID", lp_lngLibraryID)
    If l_strPathToFile = "" Then l_strPathToFile = GetData("library", "subtitle", "libraryID", lp_lngLibraryID)
Else
    If l_strPathToFile = "" Then l_strPathToFile = GetData("library", "subtitle", "libraryID", lp_lngLibraryID)
End If
l_strFilename = GetData("events", "clipfilename", "eventID", lp_lngEventID)
If Trim(" " & GetData("events", "altlocation", "eventID", lp_lngEventID)) <> "" Then l_strPathToFile = l_strPathToFile & "\" & GetData("events", "altlocation", "eventID", lp_lngEventID)
l_strPathToFile = l_strPathToFile & "\" & l_strFilename

On Error GoTo VERIFYFAILED

Set FSO = New Scripting.FileSystemObject

If FSO.FileExists(l_strPathToFile) Then
    API_OpenFile l_strPathToFile, l_lngFileHandle, l_curFileSize
    API_CloseFile l_lngFileHandle
    SetData "events", "bigfilesize", "eventID", lp_lngEventID, l_curFileSize
    SetData "events", "soundlay", "eventID", lp_lngEventID, Format(Now, "YYYY-MM-DD HH:NN:SS")
    VerifyFile = True
ElseIf FSO.FolderExists(l_strPathToFile) Then
    l_curFileSize = GetDirectorySize(l_strPathToFile)
    SetData "events", "bigfilesize", "eventID", lp_lngEventID, l_curFileSize
    SetData "events", "soundlay", "eventID", lp_lngEventID, Format(Now, "YYYY-MM-DD HH:NN:SS")
    SetData "events", "clipformat", "eventID", lp_lngEventID, "Folder"
    VerifyFile = True
Else
    SetData "events", "bigfilesize", "eventID", lp_lngEventID, "Null"
    SetData "events", "soundlay", "eventID", lp_lngEventID, "Null"
    VerifyFile = False
End If

END_FUNCTION:

Exit Function

VERIFYFAILED:
VerifyFile = False
Resume END_FUNCTION

End Function

Function GetCETASetting(lp_strSettingName As String) As Variant

GetCETASetting = GetData("setting", "value", "name", lp_strSettingName)

End Function

Function GetCount(lp_strSQL As String) As Double

Screen.MousePointer = vbHourglass

Dim l_conSearch As ADODB.Connection
Dim l_rstSearch As ADODB.Recordset

Set l_conSearch = New ADODB.Connection
Set l_rstSearch = New ADODB.Recordset

l_conSearch.ConnectionString = g_strCetaConnection
l_conSearch.Open

With l_rstSearch
     .CursorLocation = adUseClient
     .LockType = adLockBatchOptimistic
     .CursorType = adOpenDynamic
     .Open lp_strSQL, l_conSearch, adOpenStatic, adLockReadOnly
End With

l_rstSearch.ActiveConnection = Nothing

If l_rstSearch.EOF Then
    GetCount = 0
Else

    If Not IsNull(l_rstSearch(0)) Then
        GetCount = Val(l_rstSearch(0))
    Else
        GetCount = 0
    End If
End If

l_rstSearch.Close
Set l_rstSearch = Nothing

l_conSearch.Close
Set l_conSearch = Nothing

Screen.MousePointer = vbDefault

End Function


