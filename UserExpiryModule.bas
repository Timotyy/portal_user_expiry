Attribute VB_Name = "UserExpiryModule"
Option Explicit
Dim g_strConnection As String

Const g_strSMTPServer = "jca-aspera.jcatv.co.uk"
Const g_strSMTPUserName = "" ' "aria@freenetname.co.uk"
Const g_strSMTPPassword = "" '"Chorale1"
Const g_strEmailFooter = "Media Window at MX1"
Const g_strFullUSerName = "MX1 London Operations"

Public g_strAdministratorEmailAddress As String
Public g_strAdministratorEmailName As String
Public g_strBookingsEmailAddress As String
Public g_strBookingsEmailName As String

Public cnn As ADODB.Connection, cnn2 As ADODB.Connection, rst As ADODB.Recordset, rstCompany As ADODB.Recordset, rstUser As ADODB.Recordset, rstOtherUsers As ADODB.Recordset
Public SQL As String, l_strCommandLine As String, l_strEmail As String, l_strEmailFooter As String, l_lngExpiryHours As Long, l_lngCompanyID As Long
Public l_datNow As Date, l_datExpiry As Date, l_lngPortalUserID As Long, l_lngExpiry As Long, l_strcetaclientcode As String, l_lngCount As Long
Public l_lngClipID As Long, l_strClipFilename As String, rstClip As ADODB.Recordset, rstTemp As ADODB.Recordset, l_lngNewClipID As Long
Public FSO As Scripting.FileSystemObject, l_strFilePath As String, l_lngOldLibraryID As Long, l_lngStoreLibraryID As Long, l_strStoreBarcode As String, l_strStoreFilePath As String
Public l_strEmailTo As String, l_strEmailToName As String, l_strEmailCC As String, l_strEmailCCName As String
Public l_blnInformUser24hours As Boolean, l_blnInformUser120hours As Boolean, l_strEmailSubject As String, l_strEmailFileList As String, l_strEmailFileList120 As String

Sub Main()

Set FSO = New Scripting.FileSystemObject

Set cnn = New ADODB.Connection

l_strCommandLine = Command()
g_strConnection = "DRIVER={ODBC Driver 13 for SQL Server};SERVER=lon-bssql-01.office.refine-group.com;DATABASE=cetasoft;UID=Web_User;PWD=Web_User_Password1903"
cnn.Open g_strConnection

'Set up the default email varaibles for use with notification emails
SetDefaultEmailParameters

l_datNow = Format(Now, "dd/mmm/yy")

Set rstCompany = New ADODB.Recordset
Set rstUser = New ADODB.Recordset
Set rstOtherUsers = New ADODB.Recordset
Set rstClip = New ADODB.Recordset

'Check for items to be expired under the AutoDelete Folder system
Debug.Print "Expire Autodelete Files"
ExpireAutoDeleteFiles

If InStr(1, l_strCommandLine, "/MiniCETA", vbTextCompare) = 0 Then
    'Check for active portal users to be expired. Will happen if their expiry date has passed, or if they are older since actrivation than the defauilt lifetime for that company (30 dyas unless specified) and /donotexpire is not set
    'Debug.Print "Expire Users"
    'ExpirePortalUsers
    
    'Check for portal clip permissions to be expired under the /hours=xx rule. Files will be deleted if nobody else needs them
    'Debug.Print "Expire Hours"
    'ExpireHoursRule
    
    'Check for permissions that have expired under the permissionend being earlier than now. Files will be deleted if nobody else needs them.
    'Debug.Print "Expire Permission End"
    'ExpirePermissionEnd
    
    'Check for Portal permissions to be expired under the /perms=xx rule. Does NOT delete files.
    'Debug.Print "Expire Perms Rule"
    'ExpirePermsRule
    
    'Check for Quotes (and Orders) to be expired under the /expirequotes rule
    'Debug.Print "Expire Quotes"
    'ExpireQuotes
    
    'Check for Jobs that need notification e-mails sent out under the /pendingorders rule.
    'NotifyNonDeliveredOrders
    
    'Check for files that were 'Deleted' by Content Delivery Admin users who have that permission, more than a month ago, that are still hidden from web, and actually delete them
    'Debug.Print "Notify Non-Delivery Orders"
    'ExpireWebDeletedFiles
    
End If

'Check for any empty date folders on the two BBC-Hotfolder Aspera system drives and delete them
Debug.Print "Check and Delete Empty Folders"
CheckAndDeleteEmptyFolders

'Clean up duff file records
SQL = "UPDATE events SET system_deleted = 1 WHERE libraryID IN (SELECT libraryID FROM library WHERE format = 'DISCSTORE' AND system_deleted = 0) AND clipfilename is null and system_deleted = 0"
cnn.Execute SQL

cnn.Close

Set rst = Nothing
Set rstCompany = Nothing
Set rstUser = Nothing
Set rstOtherUsers = Nothing
Set rstClip = Nothing
Set cnn = Nothing
Beep
End Sub

Function GetData(lp_strTableName As String, lp_strFieldToReturn As String, lp_strFieldToSearch As String, lp_varCriterea As Variant) As Variant
        '<EhHeader>
        On Error GoTo GetData_Err
        '</EhHeader>


    Dim l_strSQL As String
    l_strSQL = "SELECT " & lp_strFieldToReturn & " FROM " & lp_strTableName & " WHERE " & lp_strFieldToSearch & " = '" & (lp_varCriterea) & "'"

    Dim l_rstGetData As New ADODB.Recordset
    Dim l_con As New ADODB.Connection

    l_con.Open g_strConnection
    l_rstGetData.Open l_strSQL, l_con, adOpenStatic, adLockReadOnly

    If Not l_rstGetData.EOF Then
    
        If Not IsNull(l_rstGetData(lp_strFieldToReturn)) Then
            GetData = l_rstGetData(lp_strFieldToReturn)
            GoTo Proc_CloseDB
        End If

    Else
        GoTo Proc_CloseDB

    End If

    Select Case l_rstGetData.Fields(lp_strFieldToReturn).Type
    Case adChar, adVarChar, adVarWChar, 201, 203
        GetData = ""
    Case adBigInt, adBinary, adBoolean, adCurrency, adDecimal, adDouble, adInteger
        GetData = 0
    Case adDate, adDBDate, adDBTime, adDBTimeStamp
        GetData = 0
    Case Else
        GetData = False
    End Select

Proc_CloseDB:

    On Error Resume Next

    l_rstGetData.Close
    Set l_rstGetData = Nothing

    l_con.Close
    Set l_con = Nothing


        '<EhFooter>
        Exit Function

GetData_Err:
        MsgBox Err.Description & vbCrLf & _
               "in prjJCAFileManager.modTest.GetData " & _
               "at line " & Erl, _
               vbExclamation + vbOKOnly, "Application Error"
        Resume Next
        '</EhFooter>
End Function

Function GetDataSQL(lp_strSQL As String) As Variant

On Error GoTo Proc_GetData_Error

Dim l_rstGetData As New ADODB.Recordset
Dim l_conGetData As New ADODB.Connection

l_conGetData.Open g_strConnection
l_rstGetData.Open lp_strSQL, l_conGetData, adOpenStatic, adLockReadOnly

If Not l_rstGetData.EOF Then
    
    If Not IsNull(l_rstGetData(0)) Then
        GetDataSQL = l_rstGetData(0)
    Else
        Select Case l_rstGetData.Fields(0).Type
        Case adChar, adVarChar, adVarWChar, 201
            GetDataSQL = ""
        Case adBigInt, adBinary, adBoolean, adCurrency, adDecimal, adDouble, adInteger
            GetDataSQL = 0
        Case adDate, adDBDate, adDBTime, adDBTimeStamp
            GetDataSQL = 0
        Case Else
            GetDataSQL = False
        End Select
    End If

End If

l_rstGetData.Close
Set l_rstGetData = Nothing

l_conGetData.Close
Set l_conGetData = Nothing

Exit Function

Proc_GetData_Error:

GetDataSQL = ""

End Function
Sub SendSMTPMail(ByVal lp_strEmailToAddress As String, ByVal lp_strEmailToName As String, ByVal lp_strSubject As String, ByVal lp_strBody As String, ByVal lp_strAttachment As String, _
ByVal lp_blnNoGlobalFooter As Boolean, ByVal lp_strCopyToEmailAddress As String, ByVal lp_strCopyToName As String, Optional ByVal lp_strEmailFooter As String, _
Optional ByVal lp_strBCCAddress As String)

Dim SQL As String, cnn2 As ADODB.Connection

If lp_blnNoGlobalFooter = True Then
    lp_strBody = lp_strBody & vbCrLf & vbCrLf & lp_strEmailFooter
Else
    lp_strBody = lp_strBody & vbCrLf & vbCrLf & g_strEmailFooter
End If

SQL = "INSERT INTO emailmessage (emailTo, emailCC, emailBCC, emailSubject, emailBodyPlain, emailAttachmentPath, cuser, muser, SendViaService) VALUES ("
SQL = SQL & "'" & QuoteSanitise(lp_strEmailToAddress) & "', "
SQL = SQL & IIf(lp_strCopyToEmailAddress <> "", "'" & QuoteSanitise(lp_strCopyToEmailAddress) & "', ", "Null, ")
SQL = SQL & IIf(lp_strBCCAddress <> "", "'" & QuoteSanitise(lp_strBCCAddress) & "', ", "Null, ")
SQL = SQL & "'" & QuoteSanitise(lp_strSubject) & "', "
SQL = SQL & "'" & QuoteSanitise(lp_strBody) & "', "
SQL = SQL & IIf(lp_strAttachment <> "", "'" & QuoteSanitise(lp_strAttachment) & "', ", "Null, ")
SQL = SQL & "'Aspera', "
SQL = SQL & "'Aspera', "
SQL = SQL & "1);"

'On Error Resume Next
Debug.Print SQL
Set cnn2 = New ADODB.Connection
cnn2.Open g_strConnection
cnn2.Execute SQL
cnn2.Close
Set cnn2 = Nothing
'On Error GoTo 0

End Sub

Function QuoteSanitise(lp_strText) As String
        '<EhHeader>
        On Error GoTo QuoteSanitise_Err
        '</EhHeader>


        Dim l_strNewText As String

        Dim l_strOldText As String

        Dim l_intLoop    As Integer

         l_strNewText = ""
    
         If IsNull(lp_strText) Then
            l_strOldText = ""
            l_strNewText = l_strOldText
            lp_strText = ""
        Else
            l_strOldText = lp_strText
        End If
    
        If InStr(lp_strText, "'") <> 0 Then
        
            For l_intLoop = 1 To Len(l_strOldText)
            
                If Mid(l_strOldText, l_intLoop, 1) = "'" Then
                    l_strNewText = l_strNewText & "''"
                Else
                    l_strNewText = l_strNewText & Mid(l_strOldText, l_intLoop, 1)
                End If
            
            Next

        Else
            l_strNewText = lp_strText
        End If
    
        If Right(l_strNewText, 1) = "/" Or Right(l_strNewText, 1) = "\" Then
            l_strNewText = Left(l_strNewText, Len(l_strNewText) - 1)
        End If
    
        QuoteSanitise = l_strNewText
    

        '<EhFooter>
        Exit Function

QuoteSanitise_Err:
        MsgBox Err.Description & vbCrLf & _
               "in prjJCAFileManager.modTest.QuoteSanitise " & _
               "at line " & Erl, _
               vbExclamation + vbOKOnly, "Application Error"
        Resume Next
        '</EhFooter>
End Function

Public Function FormatSQLDate(lDate As Variant)
    
    'TVCodeTools ErrorEnablerStart
    On Error GoTo PROC_ERR
    'TVCodeTools ErrorEnablerEnd
    
    If IsNull(lDate) Then
        FormatSQLDate = Null
        Exit Function
    End If
    If lDate = "" Then
        FormatSQLDate = Null
        Exit Function
    End If
    If Not IsDate(lDate) Then
        FormatSQLDate = Null
        Exit Function
    
    End If
    
    FormatSQLDate = Year(lDate) & "/" & Month(lDate) & "/" & Day(lDate) & " " & Hour(lDate) & ":" & Minute(lDate) & ":" & Second(lDate)
    
    'TVCodeTools ErrorHandlerStart
PROC_EXIT:
    Exit Function
    
PROC_ERR:
    MsgBox Err.Description
    Resume PROC_EXIT
    'TVCodeTools ErrorHandlerEnd
    
End Function

Public Sub ExpirePortalUsers()

On Error GoTo ErrorNotificationExpirePortalUsers


rst.Open "SELECT * FROM portaluser WHERE activeuser <> 0;", cnn, 3, 3
If rst.RecordCount > 0 Then
    rst.MoveFirst
    While Not rst.EOF
    
        l_lngExpiry = 0
        l_lngPortalUserID = rst("portaluserID")
        l_strcetaclientcode = GetData("company", "cetaclientcode", "companyID", Val(rst("companyID")))
        If InStr(l_strcetaclientcode, "/donotexpire") <= 0 Then
            If rst("donotexpire") = 0 Then
                If InStr(l_strcetaclientcode, "/EXP=") > 0 Then
                    l_lngCount = InStr(l_strcetaclientcode, "/EXP=")
                    l_lngExpiry = Val(Mid(l_strcetaclientcode, l_lngCount + 5))
                Else
                    l_lngExpiry = 30
                End If
            End If
        End If
        l_datExpiry = DateAdd("d", -l_lngExpiry, l_datNow)
        If l_lngExpiry <> 0 Then
            If rst("dateactivated") < l_datExpiry Then
                rst("activeuser") = 0
                rst.Update
                
                l_strEmailFooter = GetData("company", "portalemailsubject", "companyID", Val(rst("companyID")))
                If l_strEmailFooter = "" Then l_strEmailFooter = g_strEmailFooter
                
                l_strEmail = "The Content Delivery user '" & rst("fullname") & "' has been expired after " & l_lngExpiry & " days." & vbCrLf
                l_strEmail = l_strEmail & "The user can be re-activated through the Content Delivery Administration website at http://cd-admin.mx1.com." & vbCrLf
                If Trim(" " & rst("administratoremail")) <> "" Then
                    SendSMTPMail Trim(" " & rst("administratoremail")), "Content Delivery Administrator", "Content Delivery User Expired", l_strEmail, "", True, "", "", l_strEmailFooter
                    l_strEmail = l_strEmail & "The Company was: " & GetData("company", "name", "companyID", Val(rst("companyID"))) & vbCrLf
                    SendSMTPMail g_strAdministratorEmailAddress, g_strAdministratorEmailName, "Content Delivery User Expired", l_strEmail, "", True, "", "", l_strEmailFooter
                End If
            End If
        End If
        If Not IsNull(rst("expirydate")) Then
            If rst("expirydate") < Now Then
                rst("activeuser") = 0
                rst.Update
                
                l_strEmailFooter = GetData("company", "portalemailsubject", "companyID", Val(rst("companyID")))
                If l_strEmailFooter = "" Then l_strEmailFooter = g_strEmailFooter
                
                l_strEmail = "The Content Delivery user '" & rst("fullname") & "' has been expired having reached the set Expiry date." & vbCrLf
                l_strEmail = l_strEmail & "The user can be re-activated through the Content Delivery Administration website at http://cd-admin.mx1.com." & vbCrLf
                If Trim(" " & rst("administratoremail")) <> "" Then
                    SendSMTPMail Trim(" " & rst("administratoremail")), "Content Delivery Administrator", "Content Delivery User Expired", l_strEmail, "", True, "", "", l_strEmailFooter
                    l_strEmail = l_strEmail & "The Company was: " & GetData("company", "name", "companyID", Val(rst("companyID"))) & vbCrLf
                    SendSMTPMail g_strAdministratorEmailAddress, g_strAdministratorEmailName, "Content Delivery Expired", l_strEmail, "", True, "", "", l_strEmailFooter
                End If
            End If
        End If
        rst.MoveNext
    
    Wend
    
End If
rst.Close
Exit Sub

ErrorNotificationExpirePortalUsers:
SendSMTPMail g_strAdministratorEmailAddress, "", "ExpirePortalUsers generated an error", "Error Number: " & Err.Number & vbCrLf & "Error Descrtipion: " & Err.Description & vbCrLf, "", True, "", "", ""

End Sub

Public Sub ExpireHoursRule()

Dim l_strSQL As String

On Error GoTo ErrorNotificationExpireHoursRule

rstCompany.Open "SELECT companyID, cetaclientcode FROM company WHERE cetaclientcode LIKE '%hours=%';", cnn, 3, 3
If rstCompany.RecordCount > 0 Then
    rstCompany.MoveFirst
    While Not rstCompany.EOF
        l_lngCount = InStr(rstCompany("cetaclientcode"), "/hours=")
        l_lngExpiryHours = Val(Mid(rstCompany("cetaclientcode"), l_lngCount + 7))
        If l_lngExpiryHours > 0 Then
            l_lngCompanyID = rstCompany("companyID")
            rstUser.Open "SELECT * FROM portaluser WHERE companyID = " & l_lngCompanyID & ";", cnn, 3, 3
            If rstUser.RecordCount > 0 Then
                rstUser.MoveFirst
                While Not rstUser.EOF
                    l_blnInformUser24hours = False
                    l_blnInformUser120hours = False
                    l_strEmailFileList = ""
                    l_strEmailFileList120 = ""
                    rst.Open "SELECT * FROM portalpermission WHERE portaluserID = " & rstUser("portaluserID") & ";", cnn, 3, 3
                    If rst.RecordCount > 0 Then
                        rst.MoveFirst
                        While Not rst.EOF
                            If DateAdd("h", l_lngExpiryHours, rst("dateassigned")) <= Now Then
                                l_lngClipID = rst("eventID")
                                l_strClipFilename = GetData("events", "clipfilename", "eventID", l_lngClipID)
                                SQL = "INSERT INTO portalpermissionexpired (eventID, portaluserID, fullname, mediareference, approval, dateassigned, assignedby, permissionstart, permissionend, dateexpired) VALUES ("
                                SQL = SQL & l_lngClipID & ", "
                                SQL = SQL & rst("portaluserID") & ", "
                                SQL = SQL & "'" & QuoteSanitise(rst("fullname")) & "', "
                                SQL = SQL & "'" & QuoteSanitise(rst("mediareference")) & "', "
                                SQL = SQL & "'" & QuoteSanitise(rst("approval")) & "', "
                                SQL = SQL & "'" & FormatSQLDate(rst("dateassigned")) & "', "
                                SQL = SQL & "'" & QuoteSanitise(rst("assignedby")) & "', "
                                If Not IsNull(rst("permissionstart")) Then SQL = SQL & "'" & FormatSQLDate(rst("permissionstart")) & "', " Else SQL = SQL & "NULL, "
                                If Not IsNull(rst("permissionend")) Then SQL = SQL & "'" & FormatSQLDate(rst("permissionend")) & "', " Else SQL = SQL & "NULL, "
                                SQL = SQL & "getdate());"
                                cnn.Execute SQL
                                SQL = "DELETE FROM portalpermission WHERE portalpermissionID = " & rst("portalpermissionID") & ";"
                                cnn.Execute SQL
                                
                                'Check if anybody else still needs the clip to be there, and delete it if not
                                If Val(Trim(" " & GetDataSQL("SELECT portalpermissionID FROM portalpermission WHERE eventID = " & l_lngClipID & ";"))) = 0 Then
                                    rstClip.Open "SELECT * FROM events WHERE eventID = " & l_lngClipID & ";", cnn, 3, 3
                                    If rstClip.RecordCount = 1 Then
                                        l_lngOldLibraryID = rstClip("libraryID")
                                        If l_lngOldLibraryID = 248095 Or l_lngOldLibraryID = 276356 Or l_lngOldLibraryID = 284349 Or l_lngOldLibraryID = 441212 Or l_lngOldLibraryID = 623206 Or l_lngOldLibraryID = 722000 Then
                                            If Not IsNull(rstClip("bigfilesize")) And rstClip("system_deleted") = 0 Then
                                                l_strSQL = "INSERT INTO event_file_request (eventID, NewLibraryID, event_file_request_typeID, RequestName) VALUES ("
                                                l_strSQL = l_strSQL & l_lngClipID & ", "
                                                l_strSQL = l_strSQL & l_lngOldLibraryID & ", "
                                                l_strSQL = l_strSQL & GetData("event_File_request_type", "event_file_request_typeID", "description", "Delete") & ", "
                                                l_strSQL = l_strSQL & "'Expire Hours');"
                                                Debug.Print l_strSQL
                                                cnn.Execute l_strSQL
                                                
                                                l_strEmail = "ClipID: " & l_lngClipID & ", Full Filename: " & l_strFilePath
                                                SendSMTPMail g_strAdministratorEmailAddress, g_strAdministratorEmailName, "Portal Clip Permission Expiry: Clip Physically Deleted", l_strEmail, "", True, "", "", "Portal Clip Permission Expiry"
                                            Else
                                                rstClip("system_deleted") = 1
                                                rstClip.Update
                                            End If
                                        End If
                                    End If
                                    rstClip.Close
                                End If
                            ElseIf DateAdd("h", l_lngExpiryHours - 24, rst("dateassigned")) <= Now Then
                                l_blnInformUser24hours = True
                                l_strEmailFileList = l_strEmailFileList & GetData("events", "clipfilename", "eventID", rst("eventID")) & vbCrLf
                            ElseIf l_lngExpiryHours > 120 Then
                                If DateAdd("h", l_lngExpiryHours - 120, rst("dateassigned")) <= Now And DateAdd("h", l_lngExpiryHours - 96, rst("dateassigned")) > Now Then
                                    l_blnInformUser120hours = True
                                    l_strEmailFileList120 = l_strEmailFileList120 & GetData("events", "clipfilename", "eventID", rst("eventID")) & vbCrLf
                                End If
                            End If
                            rst.MoveNext
                        Wend
                    End If
                    rst.Close
                    If l_blnInformUser24hours = True Then
                        l_strEmail = "The following materials that are assigned to you on this Media Window will expire in 24 hours:" & vbCrLf & vbCrLf & l_strEmailFileList
                        l_strEmail = l_strEmail & vbCrLf & vbCrLf & "Media Window User Full Name: " & rstUser("Fullname") & vbCrLf & _
                            "Login: " & rstUser("username") & vbCrLf & "Password: " & rstUser("password") & vbCrLf & _
                            "Email: " & rstUser("email") & vbCrLf & _
                            "Link to Media Window Site: " & GetData("company", "portalwebsiteurl", "companyID", l_lngCompanyID) & vbCrLf & vbCrLf & _
                            "This is an informational email. Please do not reply to this email." & vbCrLf
                        l_strEmailSubject = GetData("company", "portalemailsubject", "companyID", l_lngCompanyID)
                        If l_strEmailSubject = "" Then
                            l_strEmailSubject = GetData("company", "name", "companyID", l_lngCompanyID) & " Media Window Expiry Warning"
                        End If
                        SendSMTPMail rstUser("email"), rstUser("fullname"), l_strEmailSubject, l_strEmail, "", True, rstUser("administratoremail"), "", "Media Window Expiry Warning", g_strAdministratorEmailAddress
                    End If
                    If l_blnInformUser120hours = True Then
                        l_strEmail = "The following materials that are assigned to you on this Media Window will expire in 5 days:" & vbCrLf & vbCrLf & l_strEmailFileList120
                        l_strEmail = l_strEmail & vbCrLf & vbCrLf & "Media Window User Full Name: " & rstUser("Fullname") & vbCrLf & _
                            "Login: " & rstUser("username") & vbCrLf & "Password: " & rstUser("password") & vbCrLf & _
                            "Email: " & rstUser("email") & vbCrLf & _
                            "Link to Media Window Site: " & GetData("company", "portalwebsiteurl", "companyID", l_lngCompanyID) & vbCrLf & vbCrLf & _
                            "This is an informational email. Please do not reply to this email." & vbCrLf
                        l_strEmailSubject = GetData("company", "portalemailsubject", "companyID", l_lngCompanyID)
                        If l_strEmailSubject = "" Then
                            l_strEmailSubject = GetData("company", "name", "companyID", l_lngCompanyID) & " Media Window Expiry Warning"
                        End If
                        SendSMTPMail rstUser("email"), rstUser("fullname"), l_strEmailSubject, l_strEmail, "", True, rstUser("administratoremail"), "", "Media Window Expiry Warning", g_strAdministratorEmailAddress
                    End If
                    rstUser.MoveNext
                Wend
            End If
            rstUser.Close
        End If
        rstCompany.MoveNext
    Wend
End If
rstCompany.Close
Exit Sub

ErrorNotificationExpireHoursRule:
SendSMTPMail g_strAdministratorEmailAddress, "", "ExpireHoursRule generated an error", "Error Number: " & Err.Number & vbCrLf & "Error Descrtipion: " & Err.Description & vbCrLf, "", True, "", "", ""

End Sub

Public Sub ExpirePermissionEnd()

Dim l_strSQL As String

On Error GoTo ErrorNotificationExpirePermissionEnd

rst.Open "SELECT * FROM portalpermission WHERE permissionend < getdate();", cnn, 3, 3
If rst.RecordCount > 0 Then
    rst.MoveFirst
    While Not rst.EOF
        l_lngClipID = rst("eventID")
        l_strClipFilename = GetData("events", "clipfilename", "eventID", l_lngClipID)
        SQL = "INSERT INTO portalpermissionexpired (eventID, portaluserID, fullname, mediareference, approval, dateassigned, assignedby, permissionstart, permissionend, dateexpired) VALUES ("
        SQL = SQL & l_lngClipID & ", "
        SQL = SQL & rst("portaluserID") & ", "
        SQL = SQL & "'" & QuoteSanitise(rst("fullname")) & "', "
        SQL = SQL & "'" & QuoteSanitise(rst("mediareference")) & "', "
        SQL = SQL & "'" & QuoteSanitise(rst("approval")) & "', "
        SQL = SQL & "'" & FormatSQLDate(rst("dateassigned")) & "', "
        SQL = SQL & "'" & QuoteSanitise(rst("assignedby")) & "', "
        If Not IsNull(rst("permissionstart")) Then SQL = SQL & "'" & FormatSQLDate(rst("permissionstart")) & "', " Else SQL = SQL & "NULL, "
        If Not IsNull(rst("permissionend")) Then SQL = SQL & "'" & FormatSQLDate(rst("permissionend")) & "', " Else SQL = SQL & "NULL, "
        SQL = SQL & "getdate());"
        cnn.Execute SQL
        SQL = "DELETE FROM portalpermission WHERE portalpermissionID = " & rst("portalpermissionID") & ";"
        cnn.Execute SQL
        
        'Check if anybody else still needs the clip to be there, and delete it if not
        If Val(Trim(" " & GetDataSQL("SELECT portalpermissionID FROM portalpermission WHERE eventID = " & l_lngClipID & ";"))) = 0 Then
            rstClip.Open "SELECT * FROM events WHERE eventID = " & l_lngClipID & ";", cnn, 3, 3
            If rstClip.RecordCount = 1 Then
                l_lngOldLibraryID = rstClip("libraryID")
                If l_lngOldLibraryID = 248095 Or l_lngOldLibraryID = 276356 Or l_lngOldLibraryID = 284349 Or l_lngOldLibraryID = 441212 Or l_lngOldLibraryID = 623206 Or l_lngOldLibraryID = 722000 Then
                    
                    l_strSQL = "INSERT INTO event_file_request (eventID, NewLibraryID, event_file_request_typeID, RequestName) VALUES ("
                    l_strSQL = l_strSQL & l_lngClipID & ", "
                    l_strSQL = l_strSQL & l_lngOldLibraryID & ", "
                    l_strSQL = l_strSQL & GetData("event_File_request_type", "event_file_request_typeID", "description", "Delete") & ", "
                    l_strSQL = l_strSQL & "'Expire Perms');"
                    Debug.Print l_strSQL
                    cnn.Execute l_strSQL
                    
                    l_strEmail = "ClipID: " & l_lngClipID & ", Full Filename: " & l_strFilePath
                    SendSMTPMail g_strAdministratorEmailAddress, g_strAdministratorEmailName, "Portal Clip Permission Expiry: Clip Physically Deleted", l_strEmail, "", True, "", "", "Portal Clip Permission Expiry"
                    
                End If
            End If
            rstClip.Close
        End If
        rst.MoveNext
    Wend
End If
rst.Close

''Check for distribution clip permissions to be expired under the /hours=xx rule (of the company that owns the clip)
'
'rstUser.Open "SELECT * FROM distributionuser;", cnn, 3, 3
'If rstUser.RecordCount > 0 Then
'    rstUser.MoveFirst
'    While Not rstUser.EOF
'        l_blnInformUser24hours = False
'        l_blnInformUser120hours = False
'        l_strEmailFileList = ""
'        l_strEmailFileList120 = ""
'        rst.Open "SELECT * FROM distributionpermission WHERE distributionuserID = " & rstUser("distributionuserID") & ";", cnn, 3, 3
'        If rst.RecordCount > 0 Then
'            rst.MoveFirst
'            While Not rst.EOF
'                l_lngCompanyID = rst("companyID")
'                If InStr(GetData("company", "cetaclientcode", "companyID", l_lngCompanyID), "/hours") > 0 Then
'                    l_lngCount = InStr(GetData("company", "cetaclientcode", "companyID", l_lngCompanyID), "/hours=")
'                    l_lngExpiryHours = Val(Mid(GetData("company", "cetaclientcode", "companyID", l_lngCompanyID), l_lngCount + 7))
'                    If l_lngExpiryHours > 0 Then
'
'                        If DateAdd("h", l_lngExpiryHours, rst("dateassigned")) <= Now Then
'                            l_lngClipID = rst("eventID")
'                            l_strClipFilename = GetData("events", "clipfilename", "eventID", l_lngClipID)
'                            SQL = "INSERT INTO distributionpermissionexpired (eventID, distributionuserID, fullname, mediareference, approval, dateassigned, dateexpired) VALUES ("
'                            SQL = SQL & l_lngClipID & ", "
'                            SQL = SQL & rst("distributionuserID") & ", "
'                            SQL = SQL & "'" & QuoteSanitise(rst("fullname")) & "', "
'                            SQL = SQL & "'" & QuoteSanitise(rst("mediareference")) & "', "
'                            SQL = SQL & "'" & QuoteSanitise(rst("approval")) & "', "
'                            SQL = SQL & "'" & FormatSQLDate(rst("dateassigned")) & "', "
'                            SQL = SQL & "getdate());"
'                            cnn.Execute SQL
'                            SQL = "DELETE FROM distributionpermission WHERE distributionpermissionID = " & rst("distributionpermissionID") & ";"
'                            cnn.Execute SQL
'
'                            'Check if anybody else still needs the clip to be there, and delete it if not
'                            If GetDataSQL("SELECT portalpermissionID FROM portalpermission WHERE eventID = " & l_lngClipID & ";") = 0 Then
'                                If GetDataSQL("SELECT distributionpermissionID FROM distributionpermission WHERE eventID = " & l_lngClipID & ";") = 0 Then
'                                    rstClip.Open "SELECT * FROM events WHERE eventID = " & l_lngClipID & ";", cnn, 3, 3
'                                    If rstClip.RecordCount = 1 Then
'                                        l_lngOldLibraryID = rstClip("libraryID")
'                                        If l_lngOldLibraryID = 248095 Or l_lngOldLibraryID = 276356 Or l_lngOldLibraryID = 284349 Or l_lngOldLibraryID = 441212 Or l_lngOldLibraryID = 623206 Then
'
'                                            l_strSQL = "INSERT INTO event_file_request (eventID, NewLibraryID, event_file_request_typeID, RequestName) VALUES ("
'                                            l_strSQL = l_strSQL & l_lngClipID & ", "
'                                            l_strSQL = l_strSQL & l_lngOldLibraryID & ", "
'                                            l_strSQL = l_strSQL & GetData("event_File_request_type", "event_file_request_typeID", "description", "Delete") & ", "
'                                            l_strSQL = l_strSQL & "'Expire Perms');"
'                                            Debug.Print l_strSQL
'                                            cnn.Execute l_strSQL
'
'                                            l_strEmail = "ClipID: " & l_lngClipID & ", Full Filename: " & l_strFilePath
'                                            SendSMTPMail g_strAdministratorEmailAddress, g_strAdministratorEmailName, "Distribution Clip Permission Expiry: Clip Physically Deleted", l_strEmail, "", True, "", "", "Distribution Clip Permission Expiry"
'
'                                        End If
'                                    End If
'                                    rstClip.Close
'                                End If
'                            End If
'                        ElseIf DateAdd("h", l_lngExpiryHours - 24, rst("dateassigned")) <= Now Then
'                            l_blnInformUser24hours = True
'                            l_strEmailFileList = l_strEmailFileList & GetData("events", "clipfilename", "eventID", rst("eventID")) & vbCrLf
'                        ElseIf l_lngExpiryHours > 120 Then
'                            If DateAdd("h", l_lngExpiryHours - 120, rst("dateassigned")) <= Now And DateAdd("h", l_lngExpiryHours - 96, rst("dateassigned")) > Now Then
'                                l_blnInformUser120hours = True
'                                l_strEmailFileList120 = l_strEmailFileList120 & GetData("events", "clipfilename", "eventID", rst("eventID")) & vbCrLf
'                            End If
'                        End If
'
'                    End If
'                End If
'                rst.MoveNext
'            Wend
'        End If
'        rst.Close
'        If l_blnInformUser24hours = True Then
'            l_strEmail = "The following materials that are assigned to you on this Media Window will expire in 24 hours:" & vbCrLf & vbCrLf & l_strEmailFileList
'            l_strEmail = l_strEmail & vbCrLf & vbCrLf & "Media Window User Full Name: " & rstUser("Fullname") & vbCrLf & _
'                "Login: " & rstUser("username") & vbCrLf & "Password: " & rstUser("password") & vbCrLf & _
'                "Email: " & rstUser("email") & vbCrLf & _
'                "Link to Media Window Site: " & rstUser("siteaddress") & vbCrLf & vbCrLf & _
'                "This is an informational email. Please do not reply to this email." & vbCrLf
'            l_strEmailSubject = "Distribution Media Window Expiry Warning"
'            SendSMTPMail rstUser("email"), rstUser("fullname"), l_strEmailSubject, l_strEmail, "", True, rstUser("administratoremail"), "", "Distribution Media Window Expiry Warning", g_strAdministratorEmailAddress
'        End If
'        If l_blnInformUser120hours = True Then
'            l_strEmail = "The following materials that are assigned to you on this Media Window will expire in 5 days:" & vbCrLf & vbCrLf & l_strEmailFileList120
'            l_strEmail = l_strEmail & vbCrLf & vbCrLf & "Media Window User Full Name: " & rstUser("Fullname") & vbCrLf & _
'                "Login: " & rstUser("username") & vbCrLf & "Password: " & rstUser("password") & vbCrLf & _
'                "Email: " & rstUser("email") & vbCrLf & _
'                "Link to Media Window Site: " & rstUser("siteaddress") & vbCrLf & vbCrLf & _
'                "This is an informational email. Please do not reply to this email." & vbCrLf
'            l_strEmailSubject = "Distribution Media Window Expiry Warning"
'            SendSMTPMail rstUser("email"), rstUser("fullname"), l_strEmailSubject, l_strEmail, "", True, rstUser("administratoremail"), "", "Distribution Media Window Expiry Warning", g_strAdministratorEmailAddress
'        End If
'
'        rstUser.MoveNext
'    Wend
'End If
'rstUser.Close
Exit Sub

ErrorNotificationExpirePermissionEnd:
SendSMTPMail g_strAdministratorEmailAddress, "", "ExpirePermissionEnd generated an error", "Error Number: " & Err.Number & vbCrLf & "Error Descrtipion: " & Err.Description & vbCrLf, "", True, "", "", ""

End Sub

Public Sub ExpirePermsRule()

On Error GoTo ErrorNotificationExpirePermsRule

rstCompany.Open "SELECT companyID, cetaclientcode FROM company WHERE cetaclientcode LIKE '%perms=%';", cnn, 3, 3
If rstCompany.RecordCount > 0 Then
    rstCompany.MoveFirst
    While Not rstCompany.EOF
        l_lngCount = InStr(rstCompany("cetaclientcode"), "/perms=")
        l_lngExpiryHours = Val(Mid(rstCompany("cetaclientcode"), l_lngCount + 7))
        If l_lngExpiryHours > 0 Then
            l_lngCompanyID = rstCompany("companyID")
            rstUser.Open "SELECT * FROM portaluser WHERE companyID = " & l_lngCompanyID & ";", cnn, 3, 3
            If rstUser.RecordCount > 0 Then
                rstUser.MoveFirst
                While Not rstUser.EOF
                    l_blnInformUser24hours = False
                    l_blnInformUser120hours = False
                    l_strEmailFileList = ""
                    l_strEmailFileList120 = ""
                    rst.Open "SELECT * FROM portalpermission WHERE portaluserID = " & rstUser("portaluserID") & ";", cnn, 3, 3
                    If rst.RecordCount > 0 Then
                        rst.MoveFirst
                        While Not rst.EOF
                            If DateAdd("h", l_lngExpiryHours, rst("dateassigned")) <= Now Then
                                l_lngClipID = rst("eventID")
                                l_strClipFilename = GetData("events", "clipfilename", "eventID", l_lngClipID)
                                If UCase(GetData("events", "eventtype", "eventID", l_lngClipID)) <> "PROXY" Then
                                    SQL = "INSERT INTO portalpermissionexpired (eventID, portaluserID, fullname, mediareference, approval, dateassigned, assignedby, dateexpired) VALUES ("
                                    SQL = SQL & l_lngClipID & ", "
                                    SQL = SQL & rst("portaluserID") & ", "
                                    SQL = SQL & "'" & QuoteSanitise(rst("fullname")) & "', "
                                    SQL = SQL & "'" & QuoteSanitise(rst("mediareference")) & "', "
                                    SQL = SQL & "'" & QuoteSanitise(rst("approval")) & "', "
                                    SQL = SQL & "'" & FormatSQLDate(rst("dateassigned")) & "', "
                                    SQL = SQL & "'" & QuoteSanitise(rst("assignedby")) & "', "
                                    SQL = SQL & "getdate());"
                                    cnn.Execute SQL
                                    SQL = "DELETE FROM portalpermission WHERE portalpermissionID = " & rst("portalpermissionID") & ";"
                                    cnn.Execute SQL
                                End If
                            ElseIf DateAdd("h", l_lngExpiryHours - 24, rst("dateassigned")) <= Now Then
                                l_blnInformUser24hours = True
                                l_strEmailFileList = l_strEmailFileList & GetData("events", "clipfilename", "eventID", rst("eventID")) & vbCrLf
                            ElseIf l_lngExpiryHours > 120 Then
                                If DateAdd("h", l_lngExpiryHours - 120, rst("dateassigned")) <= Now And DateAdd("h", l_lngExpiryHours - 96, rst("dateassigned")) > Now Then
                                    l_blnInformUser120hours = True
                                    l_strEmailFileList120 = l_strEmailFileList120 & GetData("events", "clipfilename", "eventID", rst("eventID")) & vbCrLf
                                End If
                            End If
                            rst.MoveNext
                        Wend
                    End If
                    rst.Close
                    If l_blnInformUser24hours = True Then
                        l_strEmail = "There following materials that are assigned to you on this Media Window will expire in 24 hours:" & vbCrLf & vbCrLf & l_strEmailFileList
                        l_strEmail = l_strEmail & vbCrLf & vbCrLf & "Media Window User Full Name: " & rstUser("Fullname") & vbCrLf & _
                            "Login: " & rstUser("username") & vbCrLf & "Password: " & rstUser("password") & vbCrLf & _
                            "Email: " & rstUser("email") & vbCrLf & _
                            "Link to Media Window Site: " & GetData("company", "portalwebsiteurl", "companyID", l_lngCompanyID) & vbCrLf & vbCrLf & _
                            "This is an informational email. Please do not reply to this email." & vbCrLf
                        l_strEmailSubject = GetData("company", "portalemailsubject", "companyID", l_lngCompanyID)
                        If l_strEmailSubject = "" Then
                            l_strEmailSubject = GetData("company", "name", "companyID", l_lngCompanyID) & " Media Window Expiry Warning"
                        End If
                        SendSMTPMail rstUser("email"), rstUser("fullname"), l_strEmailSubject, l_strEmail, "", True, rstUser("administratoremail"), "", "Media Window Expiry Warning", g_strAdministratorEmailAddress
                    End If
                    If l_blnInformUser120hours = True Then
                        l_strEmail = "There following materials that are assigned to you on this Media Window will expire in 5 days:" & vbCrLf & vbCrLf & l_strEmailFileList120
                        l_strEmail = l_strEmail & vbCrLf & vbCrLf & "Media Window User Full Name: " & rstUser("Fullname") & vbCrLf & _
                            "Login: " & rstUser("username") & vbCrLf & "Password: " & rstUser("password") & vbCrLf & _
                            "Email: " & rstUser("email") & vbCrLf & _
                            "Link to Media Window Site: " & GetData("company", "portalwebsiteurl", "companyID", l_lngCompanyID) & vbCrLf & vbCrLf & _
                            "This is an informational email. Please do not reply to this email." & vbCrLf
                        l_strEmailSubject = GetData("company", "portalemailsubject", "companyID", l_lngCompanyID)
                        If l_strEmailSubject = "" Then
                            l_strEmailSubject = GetData("company", "name", "companyID", l_lngCompanyID) & " Media Window Expiry Warning"
                        End If
                        SendSMTPMail rstUser("email"), rstUser("fullname"), l_strEmailSubject, l_strEmail, "", True, rstUser("administratoremail"), "", "Media Window Expiry Warning", g_strAdministratorEmailAddress
                    End If
                    rstUser.MoveNext
                Wend
            End If
            rstUser.Close
        End If
        rstCompany.MoveNext
    Wend
End If
rstCompany.Close
Exit Sub

ErrorNotificationExpirePermsRule:
SendSMTPMail g_strAdministratorEmailAddress, "", "ExpirePermsRule generated an error", "Error Number: " & Err.Number & vbCrLf & "Error Descrtipion: " & Err.Description & vbCrLf, "", True, "", "", ""

End Sub

Public Sub ExpireQuotes()

On Error GoTo ErrorNotificationExpireQuotes

rstCompany.Open "SELECT companyID, cetaclientcode FROM company WHERE cetaclientcode LIKE '%/expirequotes%';", cnn, 3, 3
If rstCompany.RecordCount > 0 Then
    rstCompany.MoveFirst
    While Not rstCompany.EOF
        l_lngCompanyID = rstCompany("companyID")
        l_strcetaclientcode = GetData("company", "cetaclientcode", "companyID", l_lngCompanyID)
        l_lngCount = InStr(l_strcetaclientcode, "/expirequotes=")
        If l_lngCount > 0 Then
            l_strcetaclientcode = Mid(l_strcetaclientcode, l_lngCount + 14)
            l_lngCount = Val(l_strcetaclientcode)
            l_datExpiry = DateAdd("d", -l_lngCount, Now)
        Else
            l_datExpiry = DateAdd("d", -14, Now)
        End If
        
        'Run through cancelling all the ones that are older than 14 days.
        SQL = "SELECT * FROM job WHERE companyID = " & l_lngCompanyID & " AND fd_status = 'Pencil' AND createddate < '" & FormatSQLDate(l_datExpiry) & "' ORDER BY jobID;"
        Debug.Print SQL
        rst.Open SQL, cnn, 3, 3
        If rst.RecordCount > 0 Then
            rst.MoveFirst
            While Not rst.EOF
                rst("fd_status") = "Cancelled"
                rst("cancelleddate") = Now
                rst("cancelleduser") = "AUTO"
                rst("flagquote") = 0
                rst.Update
                rst.MoveNext
            Wend
        End If
        rst.Close
        
        'Then run through all the ones older than 7 days sending out the warnings.
        rstUser.Open "SELECT cetaclientcode FROM company WHERE companyID = " & l_lngCompanyID & ";", cnn, 3, 3
        If InStr(rstUser("cetaclientcode"), "/noquotesexpiremessage") <= 0 Then
        
            SQL = "SELECT * FROM job WHERE companyID = " & l_lngCompanyID & " AND fd_status = 'Pencil' AND createddate < '" & FormatSQLDate(DateAdd("d", 7, l_datExpiry)) & "' AND createddate > '" & FormatSQLDate(DateAdd("d", 6, l_datExpiry)) & "' ORDER BY jobID;"
            Debug.Print SQL
            rst.Open SQL, cnn, 3, 3
            If rst.RecordCount > 0 Then
                rst.MoveFirst
                While Not rst.EOF
                    If Not IsNull(rst("contactID")) Then
                        rstUser.Open "SELECT email, name FROM contact WHERE contactID = " & rst("contactID") & ";", cnn, 3, 3
                        If rstUser.RecordCount > 0 Then
                            rstUser.MoveFirst
                            l_strEmail = "RRsat web Order Number " & rst("jobID") & " created on " & Format(rst("createddate"), "DD mmm YY") & " has not yet been submitted to RRsat." & vbCrLf & vbCrLf
                            l_strEmail = l_strEmail & "Unsubmitted web orders are automatically deleted after " & l_lngCount & " days." & vbCrLf & vbCrLf
                            Debug.Print l_strEmail
                            SendSMTPMail rstUser("email"), rstUser("name"), "RRsat Unsubmitted Web Order", l_strEmail, "", True, "", "", "Media Window Expiry Warning", g_strAdministratorEmailAddress
                        End If
                        rstUser.Close
                    End If
                    rst.MoveNext
                Wend
            End If
            rst.Close
        End If
        rstUser.Close
        
        rstCompany.MoveNext
    Wend
End If
rstCompany.Close
Exit Sub

ErrorNotificationExpireQuotes:
SendSMTPMail g_strAdministratorEmailAddress, "", "ExpireQuotes generated an error", "Error Number: " & Err.Number & vbCrLf & "Error Descrtipion: " & Err.Description & vbCrLf, "", True, "", "", ""

End Sub

Public Sub ExpireWebDeletedFiles()

On Error GoTo ErrorNotificationExpireWebDeletedFiles

Dim l_lngCompanyID As Long, l_strcetaclientcode As String, l_lngCount As Long, l_datExpiry As Date

rstCompany.Open "SELECT companyID, cetaclientcode FROM company WHERE cetaclientcode LIKE '%/deletefiles%'", cnn, 3, 3
If rstCompany.RecordCount > 0 Then
    rstCompany.MoveFirst
    Do While Not rstCompany.EOF
        
        l_lngCompanyID = rstCompany("companyID")
        l_strcetaclientcode = GetData("company", "cetaclientcode", "companyID", l_lngCompanyID)
        l_lngCount = InStr(l_strcetaclientcode, "/WebDeleteDays=")
        If l_lngCount > 0 Then
            l_strcetaclientcode = Mid(l_strcetaclientcode, l_lngCount + 15)
            l_lngCount = Val(l_strcetaclientcode)
            l_datExpiry = DateAdd("d", -l_lngCount, Now)
        Else
            l_datExpiry = DateAdd("d", -7, Now)
        End If
        
        SQL = "SELECT * FROM events WHERE companyID = " & rstCompany("companyID") & " AND system_deleted = 0 AND libraryID IN (SELECT libraryID FROM library WHERE format = 'DISCSTORE' and system_deleted = 0) AND hidefromweb IS NOT NULL and hidefromweb <> 0 AND MediaWindowDeleteDate IS NOT NULL AND MediaWindowDeleteDate < '" & Format(l_datExpiry, "YYYY-MM-DD HH:NN:SS") & "';"
        Debug.Print SQL
        rst.Open SQL, cnn, 3, 3
        If rst.RecordCount > 0 Then
            rst.MoveFirst
            Do While Not rst.EOF
                l_lngClipID = rst("eventID")
                l_lngOldLibraryID = rst("libraryID")
                SQL = "INSERT INTO event_file_request (eventID, NewLibraryID, event_file_request_typeID, RequestName) VALUES ("
                SQL = SQL & l_lngClipID & ", "
                SQL = SQL & l_lngOldLibraryID & ", "
                SQL = SQL & GetData("event_File_request_type", "event_file_request_typeID", "description", "Delete") & ", "
                SQL = SQL & "'Expire WebDel');"
                Debug.Print SQL
                cnn.Execute SQL

                l_strEmail = "ClipID: " & l_lngClipID & ", Full Filename: " & l_strFilePath
                SendSMTPMail g_strAdministratorEmailAddress, g_strAdministratorEmailName, "Content Delivery Admin User Delete (28 days ago): Clip Physically Deleted", l_strEmail, "", True, "", "", "Portal Clip Permission Expiry"

                rst.MoveNext
            Loop
        End If
        rst.Close
        rstCompany.MoveNext
    Loop
End If
rstCompany.Close
Exit Sub

ErrorNotificationExpireWebDeletedFiles:
SendSMTPMail g_strAdministratorEmailAddress, "", "ExpireWebDeletedFiles generated an error", "Error Number: " & Err.Number & vbCrLf & "Error Descrtipion: " & Err.Description & vbCrLf, "", True, "", "", ""

End Sub

Public Sub ExpireAutoDeleteFiles()

Dim AutoDeleteDays As Long, Elements As Long, Count As Long
Dim AltLocation() As String

On Error GoTo ErrorNotificationExpireAutoDeleteFiles

rst.Open "SELECT eventID, libraryID, altlocation, clipfilename, clipreference, AutoDeleteDate, bigfilesize, system_deleted FROM events WHERE altlocation LIKE '%AUTODELETE%' AND system_deleted = 0 AND libraryID IN (SELECT libraryID FROM library WHERE format = 'DISCSTORE') ORDER BY libraryID, altlocation, clipfilename;", cnn, 3, 3
If rst.RecordCount > 0 Then
    rst.MoveFirst
    Do While Not rst.EOF
    
        'Any items with a null autodelete date have arrived here by accivent, but they need to be part of the process, so put today's date in them.
        If IsNull(rst("AutoDeleteDate")) Then
            rst("AutoDeleteDate") = Now
            rst.Update
        End If

        AltLocation = Split(rst("altlocation"), "\")
        If Left(UCase(AltLocation(0)), 10) = "AUTODELETE" Then
            AutoDeleteDays = Val(Mid(AltLocation(0), 11))
        ElseIf UBound(AltLocation) > 0 Then
            Elements = UBound(AltLocation)
            For Count = 1 To Elements
                If Left(UCase(AltLocation(Count)), 10) = "AUTODELETE" Then
                    AutoDeleteDays = Val(Mid(AltLocation(Count), 11))
                    Exit For
                End If
            Next
        End If
        If AutoDeleteDays = 0 Then AutoDeleteDays = 30
        If Not IsNull(rst("AutoDeleteDate")) Then
            If rst("autodeletedate") < DateAdd("D", -AutoDeleteDays, Now) Then
                If IsNull(rst("bigfilesize")) Then
                    rst("system_deleted") = 1
                Else
                    l_lngClipID = rst("eventID")
                    l_lngOldLibraryID = rst("libraryID")
                    SQL = "INSERT INTO event_file_request (eventID, NewLibraryID, event_file_request_typeID, RequestName) VALUES ("
                    SQL = SQL & l_lngClipID & ", "
                    SQL = SQL & l_lngOldLibraryID & ", "
                    SQL = SQL & GetData("event_File_request_type", "event_file_request_typeID", "description", "Delete") & ", "
                    SQL = SQL & "'AutoDelete" & AutoDeleteDays & "');"
                    Debug.Print SQL
                    cnn.Execute SQL
    
                    'l_strEmail = "ClipID: " & l_lngClipID & ", Full Filename: " & rst("clipfilename") & " on " & GetData("library", "barcode", "libraryID", l_lngOldLibraryID)
                    'SendSMTPMail g_strAdministratorEmailAddress, g_strAdministratorEmailName, "File AutoDelete", "AutoDelete (" & AutoDeleteDays & " day(s) after arriving in folder): Clip Physically Deleted" & vbCrLf & l_strEmail, "", True, "", ""
                End If
            End If
        Else
            rst("autodeletedate") = Format(Now, "YYYY-MM-DD HH:NN:SS")
            rst.Update
        End If
        rst.MoveNext
    Loop
End If
rst.Close
Exit Sub

ErrorNotificationExpireAutoDeleteFiles:
SendSMTPMail g_strAdministratorEmailAddress, "", "ExpireAutoDeleteFiles generated an error", "Error Number: " & Err.Number & vbCrLf & "Error Descrtipion: " & Err.Description & vbCrLf, "", True, "", "", ""

End Sub
Public Sub NotifyNonDeliveredOrders()

On Error GoTo ErrorNotificationNotifyNonDeliveredOrders

rst.Open "SELECT * FROM job WHERE donotdeliveryet <> 0 AND deadlinedate <= '" & FormatSQLDate(DateAdd("h", 48, Now)) & "' AND (fd_status = 'Confirmed' OR fd_status = 'Completed' OR fd_status = 'Rejected' OR fd_status = 'Masters Here' OR fd_status = 'In Progress' OR fd_status = 'VT Done' OR fd_status = 'On Hold');", cnn, 3, 3
If rst.RecordCount > 0 Then
    rst.MoveFirst
    While Not rst.EOF
        l_strEmail = "RRsat Job number " & rst("jobID") & " - Title: '" & Trim(" " & rst("title1")) & "' is marked as 'Do Not Deliver Yet' but is within 48 hours of reaching the Deadline date." & vbCrLf
        Debug.Print l_strEmail
        l_strEmailTo = Trim(" " & GetData("contact", "email", "contactID", rst("contactID")))
        l_strEmailToName = Trim(" " & GetData("contact", "name", "contactID", rst("contactID")))
        l_strEmailCC = Trim(" " & GetData("company", "jcacontactemail", "companyID", rst("companyID")))
        If l_strEmailTo = "" Then l_strEmailTo = g_strBookingsEmailAddress
        If l_strEmailCC = "" Then l_strEmailCC = g_strBookingsEmailAddress
        SendSMTPMail l_strEmailTo, l_strEmailToName, "RRsat 'Do Not Deliver' Job nearing Deadline Date", l_strEmail, "", True, l_strEmailCC, "", "", g_strBookingsEmailAddress
        rst.MoveNext
    Wend
End If
rst.Close
Exit Sub

ErrorNotificationNotifyNonDeliveredOrders:
SendSMTPMail g_strAdministratorEmailAddress, "", "NotifyNonDeliveredOrders generated an error", "Error Number: " & Err.Number & vbCrLf & "Error Descrtipion: " & Err.Description & vbCrLf, "", True, "", "", ""

End Sub

Public Sub SetDefaultEmailParameters()

Set rst = New ADODB.Recordset

SQL = "SELECT value FROM setting WHERE name = 'AdministratorEmailAddress';"
rst.Open SQL, cnn, 3, 3
If rst.RecordCount > 0 Then
    g_strAdministratorEmailAddress = rst("value")
End If
rst.Close
SQL = "SELECT value FROM setting WHERE name = 'AdministratorEmailName';"
rst.Open SQL, cnn, 3, 3
If rst.RecordCount > 0 Then
    g_strAdministratorEmailName = rst("value")
End If
rst.Close
SQL = "SELECT value FROM setting WHERE name = 'BookingsEmailAddress';"
rst.Open SQL, cnn, 3, 3
If rst.RecordCount > 0 Then
    g_strBookingsEmailAddress = rst("value")
End If
rst.Close
SQL = "SELECT value FROM setting WHERE name = 'BookingsEmailName';"
rst.Open SQL, cnn, 3, 3
If rst.RecordCount > 0 Then
    g_strBookingsEmailName = rst("value")
End If
rst.Close

End Sub

Public Sub CheckAndDeleteEmptyFolders()

Set rst = New ADODB.Recordset

rst.Open "SELECT * FROM EmptyFolderCheck ORDER BY EmptyFolderCheckID;", cnn, 3, 3

If rst.RecordCount > 0 Then
    rst.MoveFirst
    Do While Not rst.EOF
        l_strFilePath = GetData("library", "subtitle", "barcode", rst("EmptyFolderCheckBarcode"))
        If Trim(" " & rst("EmptyFolderCheckSubfolder")) <> "" Then
            l_strFilePath = l_strFilePath & "\" & rst("EmptyFolderCheckSubfolder")
        End If
        l_strCommandLine = "Delete_Empty_Dirs.exe """ & l_strFilePath & """"
        Debug.Print l_strCommandLine
        Shell l_strCommandLine
        rst.MoveNext
    Loop
End If
rst.Close


End Sub

